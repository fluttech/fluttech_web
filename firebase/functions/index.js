const functions = require("firebase-functions");
const admin = require("firebase-admin");
const nodemailer = require("nodemailer");
admin.initializeApp();

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user:'fluttech.test@gmail.com',
    pass:'Ez_1_Jelszo',
  },
});

exports.sendMail = functions.https.onCall(async (data, context) => {
  const email = data.email;
  const subject = data.subject;
  const message = data.message;

  if (!email) {
    console.log(`Invalid feedback. User email not found.`);
    return Promise.reject(new Error(`User email not found.`));
  }

  return sendEmail(email, subject, message);
});

async function sendEmail(email, subject, message) {
  const emailTo = 'contact@fluttech.com';
  console.log(`Email to: ${emailTo}`);
  const mailOptions = {
    from: 'fluttech.test@gmail.com',
    to: emailTo,
    subject: `Subject - ${subject}`,
    text: mailBodyText(email, subject, message),
    html: mailBody(email, subject, message),
  };

  return transporter.sendMail(mailOptions, (error, data) => {
    if (error) {
      return console.log(`Sending from ${email} error: ${error.toString()}`);
    }
    return console.log(`Email from ${email} was sent successfully`);
  });
}

const mailBody = (email, subject, message) => {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html>
             <head>
                 <meta http-equiv="Content-Type"
                 content="text/html charset=UTF-8" />
             </head>
                  <body>
                      <p>Hey Fluttech support,</p>
                      <p><b>${email}</b> just sent us an email.
                      <br>You can find all the details below:</p>
                      <br>
                      <i>
                        ${message}
                        <br>
                      </i>
                      <br>
                      <p>All the best,</p>
                      <p><b>Fluttech Firebase</b></p>
                  </body>
          </html>`;
};

const mailBodyText = (email, subject, message) => {
  return ` Hey Fluttech support, \n
    ${email}just sent an email. \n
       You can find all the details below.  \n\n

      "${message}"
      \n

      All the best, \n
      Fluttech Firebase`;
};
