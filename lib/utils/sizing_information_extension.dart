
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

extension SizingInformationExtension on SizingInformation {
  bool isMobileView(BuildContext context) {
    if (isMobile) {
      return true;
    }

    if (isTablet &&
        MediaQuery.of(context).orientation == Orientation.portrait) {
      return true;
    }

    if (isDesktop && screenSize.width < 900) {
      return true;
    }

    return false;
  }

  bool isSmallScreen() {
    return screenSize.width < 800;
  }

  bool isLargeScreen() {
    return screenSize.width > 1200;
  }

  bool isMediumScreen() {
    return screenSize.width >= 800 &&
        screenSize.width <= 1200;
  }

}

const double BACKGROUND_MARGIN = 100;
const double MAX_WIDTH = 1200;
const double RIGHT_PADDING = 70;
const double LEFT_PADDING = 140;
const double STROKE_WIDTH = 4;