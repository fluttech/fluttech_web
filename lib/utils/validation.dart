
final RegExp _emailAddressRegExp =
    RegExp(r"^([a-zA-Z0-9_\-\.\+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,63})$");

bool isValidEmailAddress(String email) {
  if (email == null || email.isEmpty) {
    return false;
  }

  var matchedEmail = _emailAddressRegExp.stringMatch(email);
  return matchedEmail == email;
}