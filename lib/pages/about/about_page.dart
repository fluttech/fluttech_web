import 'package:fluttech/pages/about/about_screen_desktop.dart';
import 'package:flutter/material.dart';

class AboutUsPage extends Page {
  static const String route = '/aboutUs/';
  final Widget navBar;

  AboutUsPage({required this.navBar}) : super(key: ValueKey('AboutUsPage'));

  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
      opaque: false,
      transitionDuration: Duration(milliseconds: 200),
      transitionsBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child,
      ) {
        return FadeTransition(
          opacity: animation,
          child: child,
        );
      },
      settings: this,
      pageBuilder: (context, animation, animation2) {
        return AboutUsScreen(
          navBar: navBar,
        );
      },
    );
  }
}
