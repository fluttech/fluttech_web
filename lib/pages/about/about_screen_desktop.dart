import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/animated_button.dart';
import 'package:fluttech/widgets/background/background.dart';
import 'package:flutter/material.dart';

class AboutUsScreen extends StatelessWidget {
  final Widget navBar;

  AboutUsScreen({
    Key? key,
    required this.navBar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          // Background(
          //   height: 2000,
          // ),
          // navbar
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 27,
              horizontal: 107.0,
            ),
            child: navBar,
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 150,
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  // intro section
                  Column(
                    children: [
                      StorySectionRow(),
                      SizedBox(height: 20),
                      // PrinciplesSection(),
                      // QuoteSection(),
                      // Footer(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class StorySectionRow extends StatelessWidget {
  const StorySectionRow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        SizedBox(
          width: LEFT_PADDING,
        ),
        Container(
          height: 650,
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 394,
                    height: 151,
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      'Our story + team',
                      style: TextStyle(
                        fontSize: 45,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                  ),
                  Container(
                    width: 450,
                    height: 270,
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  CustomPaint(
                    painter: TrianglePainter(
                      strokeColor: Colors.black,
                      strokeWidth: 6,
                      paintingStyle: PaintingStyle.stroke,
                    ),
                    child: Container(
                      height: 40,
                      width: 30,
                    ),
                  ),
                ],
              ),
              SizedBox(width: 70),
              Container(
                width: 400,
                height: 450,
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: 200,
                      height: 200,
                      color: Colors.white,
                      child: Placeholder(
                        color: const Color(0xFF000000),
                        strokeWidth: STROKE_WIDTH,
                      ),
                    ),
                    Positioned(
                      top: 50,
                      right: 40,
                      child: Container(
                        width: 200,
                        height: 200,
                        color: Colors.white,
                      ),
                    ),
                    Positioned(
                      top: 50,
                      right: 40,
                      child: Container(
                        width: 200,
                        height: 200,
                        child: Placeholder(
                          color: const Color(0xFF000000),
                          strokeWidth: STROKE_WIDTH,
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 80,
                      left: 100,
                      child: Container(
                        width: 200,
                        height: 200,
                        color: Color(0xffCB4CFD),
                      ),
                    ),
                    Positioned(
                      bottom: 100,
                      left: 80,
                      child: Container(
                        width: 200,
                        height: 200,
                        color: Colors.white,
                        child: Placeholder(
                          color: const Color(0xFF000000),
                          strokeWidth: STROKE_WIDTH,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: RIGHT_PADDING,
        ),
      ],
    );
  }
}

class PrinciplesSection extends StatelessWidget {
  const PrinciplesSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Background(
          color: Color(0xFFF4f4f4),
          // height: 800,
          blendMode: BlendMode.multiply,
        ),
        FittedBox(
          child: Container(
            height: 800,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  width: 142,
                  padding: EdgeInsets.only(left: 50),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RotatedBox(
                        quarterTurns: -1,
                        child: Text(
                          'DEV PRINCIPLES',
                          style: TextStyle(
                              fontWeight: FontWeight.w700, fontSize: 32),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 1335,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(height: 30),
                      Text(
                        'How we do it?',
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 50,
                          color: Colors.black,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      SizedBox(height: 50),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              PrinciplesItem(text: 'F', width: 400),
                              PrinciplesItem(text: 'L', width: 350),
                              PrinciplesItem(text: 'U', width: 400),
                              PrinciplesItem(text: 'T', width: 320),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              PrinciplesItem(text: 'T', width: 400),
                              PrinciplesItem(text: 'E', width: 320),
                              PrinciplesItem(text: 'C', width: 350),
                              PrinciplesItem(text: 'H', width: 400),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 50),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          AnimatedButton(
                            width: 350,
                            height: 101,
                            borderSize: 10,
                            text: 'Technology',
                            textSize: 20,
                          ),
                          AnimatedButton(
                            width: 80,
                            height: 80,
                            borderSize: 10,
                            text: 'by',
                            textSize: 20,
                          ),
                          AnimatedButton(
                            width: 350,
                            height: 101,
                            borderSize: 10,
                            text: 'Developers',
                            textSize: 20,
                          ),
                        ],
                      ),
                      SizedBox(height: 25),
                    ],
                  ),
                ),
                SizedBox(
                  width: 150.0,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class PrinciplesItem extends StatelessWidget {
  final String text;
  final double width;

  const PrinciplesItem({
    Key? key,
    required this.text,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          text,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 80,
            color: Colors.black,
          ),
        ),
        SizedBox(
          width: 30.0,
        ),
        Container(
          color: Colors.black,
          width: width,
          height: 15,
        ),
      ],
    );
  }
}

class TrianglePainter extends CustomPainter {
  final Color strokeColor;
  final PaintingStyle paintingStyle;
  final double strokeWidth;

  TrianglePainter(
      {this.strokeColor = Colors.black,
      this.strokeWidth = 3,
      this.paintingStyle = PaintingStyle.stroke});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = strokeColor
      ..strokeWidth = strokeWidth
      ..style = paintingStyle;

    canvas.drawPath(getTrianglePath(size.width, size.height), paint);
  }

  Path getTrianglePath(double x, double y) {
    return Path()
      ..moveTo(0, 0)
      ..lineTo(0, y)
      ..lineTo(x, y / 2)
      ..lineTo(0, 4);
  }

  @override
  bool shouldRepaint(TrianglePainter oldDelegate) {
    return oldDelegate.strokeColor != strokeColor ||
        oldDelegate.paintingStyle != paintingStyle ||
        oldDelegate.strokeWidth != strokeWidth;
  }
}
