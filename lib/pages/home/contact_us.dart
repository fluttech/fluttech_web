import 'package:fluttech/application.dart';
import 'package:fluttech/bloc/cloud_functions/cloud_functions_bloc.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/utils/validation.dart';
import 'package:fluttech/widgets/animated_button.dart';
import 'package:fluttech/widgets/background/background.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_builder/responsive_builder.dart';

class QuoteSection extends StatefulWidget {
  final String subject;

  const QuoteSection({
    Key? key,
    this.subject = '',
  }) : super(key: key);

  @override
  _QuoteSectionState createState() => _QuoteSectionState();
}

class _QuoteSectionState extends State<QuoteSection> {
  late TextEditingController _emailController;
  late TextEditingController _subjectController;
  late TextEditingController _messageController;
  late FToast fToast;

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    _subjectController = TextEditingController(
      text: widget.subject,
    );
    _messageController = TextEditingController();

    var updateStateListener = () {
      setState(() {});
    };
    _emailController.addListener(updateStateListener);
    _subjectController.addListener(updateStateListener);
    _messageController.addListener(updateStateListener);

    fToast = FToast();
    fToast.init(context);
  }

  @override
  void didUpdateWidget(covariant QuoteSection oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {
      _subjectController.text = widget.subject;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _subjectController.dispose();
    _messageController.dispose();
  }

  void _showToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Color(0xFFfff8e1),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Flexible(
            child: Text(
              S.of(context).quote_thanks,
              style: TextStyle(
                fontSize: 18,
                height: 1.5,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.CENTER,
      toastDuration: Duration(seconds: 5),
    );
  }

  void _doSendEmail(BuildContext context) {
    BlocProvider.of<CloudFunctionsBloc>(context).add(
      SendEmail(
        email: _emailController.text,
        subject: _subjectController.text,
        message: _messageController.text,
      ),
    );

    _resetForms();
  }

  Widget _buildTitle(SizingInformation sizingInformation) {
    return SelectableText(
      S.of(context).quote_title,
      style: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: sizingInformation.isSmallScreen() ? 38 : 42,
        color: Colors.black,
      ),
    );
  }

  Widget _buildDesktopContactUs(
    BuildContext context,
    SizingInformation sizingInformation,
    CloudFunctionsState cloudFunctionsState,
  ) {
    var localWidth = sizingInformation.localWidgetSize.width;
    return Background(
      color: Color(0xFF32FFFC),
      blendMode: BlendMode.darken,
      height: 520,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(width: LEFT_PADDING),
          Container(
            width: localWidth - 2 * LEFT_PADDING,
            height: 520,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 20),
                _buildTitle(sizingInformation),
                SizedBox(height: 50),
                Wrap(
                  alignment: WrapAlignment.center,
                  spacing: 20,
                  runSpacing: 20,
                  children: [
                    Container(
                      height: 50,
                      width: 400,
                      child: CustomTextField(
                        controller: _emailController,
                        hint: S.of(context).quote_email,
                        textInputType: TextInputType.emailAddress,
                        validator: (email) {
                          if (email == null || email.isEmpty) {
                            return S.of(context).empty_email_error;
                          }
                          if (!isValidEmailAddress(email)) {
                            return S.of(context).invalid_email_error;
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      height: 50,
                      width: 400,
                      child: CustomTextField(
                        controller: _subjectController,
                        hint: S.of(context).quote_subject,
                        textInputType: TextInputType.text,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 25),
                Container(
                  width: localWidth >= 1100 ? 820 : 400,
                  child: CustomTextField(
                    maxLines: 6,
                    minLines: 6,
                    controller: _messageController,
                    hint: S.of(context).quote_message,
                    textInputType: TextInputType.text,
                  ),
                ),
                SizedBox(height: 40),
                AnimatedButton(
                  width: 200,
                  height: 55,
                  borderSize: STROKE_WIDTH,
                  text: S.of(context).quote_engage,
                  textSize: 16,
                  onPressed: _isFormComplete()
                      ? () {
                          _doSendEmail(context);
                        }
                      : null,
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
          SizedBox(width: LEFT_PADDING),
        ],
      ),
    );
  }

  Widget _buildMobileContactUs(SizingInformation sizingInformation) {
    return Background(
      color: Color(0xFF32FFFC),
      blendMode: BlendMode.darken,
      height: 600,
      child: Container(
        height: 600,
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 55.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildTitle(sizingInformation),
            SizedBox(
              height: 60,
            ),
            Container(
              height: 50,
              child: CustomTextField(
                controller: _emailController,
                hint: S.of(context).quote_email,
                textInputType: TextInputType.emailAddress,
                validator: (email) {
                  if (email == null || email.isEmpty) {
                    return S.of(context).empty_email_error;
                  }
                  if (!isValidEmailAddress(email)) {
                    return S.of(context).invalid_email_error;
                  }
                  return null;
                },
              ),
            ),
            SizedBox(height: 25),
            Container(
              height: 50,
              child: CustomTextField(
                controller: _subjectController,
                hint: S.of(context).quote_subject,
                textInputType: TextInputType.text,
              ),
            ),
            SizedBox(height: 25),
            Container(
              child: CustomTextField(
                maxLines: 6,
                minLines: 6,
                controller: _messageController,
                hint: S.of(context).quote_message,
                textInputType: TextInputType.text,
              ),
            ),
            SizedBox(height: 50),
            AnimatedButton(
              width: 160,
              height: 41,
              borderSize: STROKE_WIDTH,
              text: S.of(context).quote_engage,
              textSize: 10,
              onPressed: _isFormComplete()
                  ? () {
                      _doSendEmail(context);
                    }
                  : null,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CloudFunctionsBloc, CloudFunctionsState>(
      listener: (context, state) {
        if (state is EmailSent) {
          _showToast();
        }
      },
      builder: (context, cloudFunctionsState) {
        return ResponsiveBuilder(
          builder: (context, sizingInformation) {
            if (sizingInformation.isSmallScreen()) {
              return _buildMobileContactUs(sizingInformation);
            }
            return _buildDesktopContactUs(
              context,
              sizingInformation,
              cloudFunctionsState,
            );
          },
        );
      },
    );
  }

  bool _isFormComplete() {
    return _emailController.text.isNotEmpty &&
        _subjectController.text.isNotEmpty &&
        _messageController.text.isNotEmpty &&
        isValidEmailAddress(_emailController.text);
  }

  void _resetForms() {
    setState(() {
      _emailController.clear();
      _subjectController.clear();
      _messageController.clear();
    });
  }
}

class CustomTextField extends StatelessWidget {
  final TextEditingController controller;
  final TextInputType textInputType;
  final FormFieldValidator<String>? validator;
  final String hint;
  final int maxLines;
  final int minLines;

  const CustomTextField({
    Key? key,
    required this.controller,
    required this.textInputType,
    required this.hint,
    this.validator,
    this.maxLines = 1,
    this.minLines = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black45,
          width: STROKE_WIDTH / 2,
        ),
        color: Colors.white,
      ),
      child: TextFormField(
        controller: controller,
        autocorrect: false,
        minLines: minLines,
        maxLines: maxLines,
        style: TextStyle(
          color: Colors.black,
        ),
        keyboardType: textInputType,
        decoration: InputDecoration(
          hintText: hint,
          border: InputBorder.none,
          contentPadding: EdgeInsets.symmetric(
            horizontal: 10.0,
            vertical: maxLines != 1 ? 10 : 0,
          ),
          hintStyle: TextStyle(
            color: Colors.grey.shade400,
          ),
        ),
        validator: validator,
        autovalidateMode: AutovalidateMode.onUserInteraction,
      ),
    );
  }
}
