import 'package:fluttech/application.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/animated_lines.dart';
import 'package:fluttech/widgets/custom_placeholder.dart';
import 'package:fluttech/widgets/animated_button.dart';
import 'package:fluttech/widgets/animated_placeholder.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

const double MAX_LOCAL_WIDTH = 1000;

class WhatWeDo extends StatelessWidget {
  final VoidCallback onPressed;

  const WhatWeDo({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  Widget _buildTitle(BuildContext context, double size) {
    return SelectableText(
      S.of(context).what_we_do_title,
      style: TextStyle(
        fontSize: size,
        fontWeight: FontWeight.w800,
      ),
    );
  }

  Widget _buildWhatWeDoMessage(BuildContext context, double size) {
    return SelectableText(
      S.of(context).what_we_do,
      style: TextStyle(
        fontSize: size,
        height: 1.5,
        fontWeight: FontWeight.w400,
      ),
    );
  }

  Widget _desktopRow1(
      BuildContext context, SizingInformation sizingInformation) {
    var localWidgetWidth = sizingInformation.localWidgetSize.width;
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        SizedBox(width: LEFT_PADDING),
        Container(
          width: 320,
          height: 151,
          alignment: Alignment.bottomLeft,
          child: _buildTitle(context, 42),
        ),
        Spacer(),
        AnimatedPlaceHolder(
          width: 170,
          height: 150,
        ),
        if (localWidgetWidth >= MAX_LOCAL_WIDTH) ...[
          SizedBox(width: 20.0),
          CustomPlaceHolder(
            color: Color(0xFF000000),
            shadowColor: Colors.grey,
            fallbackWidth: 250.0,
            fallbackHeight: 150.0,
            strokeWidth: STROKE_WIDTH,
          ),
        ],
        SizedBox(width: RIGHT_PADDING),
      ],
    );
  }

  Widget _desktopRow2(
    BuildContext context,
    SizingInformation sizingInformation,
  ) {
    var localWidgetWidth = sizingInformation.localWidgetSize.width;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        SizedBox(
          width: LEFT_PADDING,
        ),
        Container(
          width: 380,
          height: 180,
          padding: EdgeInsets.only(right: 10.0),
          alignment: Alignment.topLeft,
          child: _buildWhatWeDoMessage(context, 18),
        ),
        Spacer(),
        if (localWidgetWidth >= MAX_LOCAL_WIDTH + 100)
          CustomPlaceHolder(
            color: Color(0xFF000000),
            shadowColor: Colors.grey,
            fallbackWidth: 220.0,
            fallbackHeight: 150.0,
            strokeWidth: STROKE_WIDTH,
          ),
        SizedBox(
          width: 20.0,
        ),
        if (!sizingInformation.isSmallScreen())
          Container(
            width: 190.0,
            height: 155.0,
            padding: EdgeInsets.only(
              top: 10.0,
            ),
            child: ServicesAndProducts(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
          ),
        SizedBox(
          width: localWidgetWidth >= MAX_LOCAL_WIDTH + 100
              ? RIGHT_PADDING + 60
              : RIGHT_PADDING,
        ),
      ],
    );
  }

  Widget _desktopRow3(
    BuildContext context,
    SizingInformation sizingInformation,
  ) {
    var localWidgetWidth = sizingInformation.localWidgetSize.width;
    var circleSize = 20.0;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        SizedBox(width: LEFT_PADDING),
        AnimatedButton(
          width: 200,
          height: 55,
          borderSize: STROKE_WIDTH,
          text: S.of(context).see_projects,
          textSize: 16,
          onPressed: onPressed,
        ),
        SizedBox(width: 40.0),
        if (sizingInformation.isLargeScreen())
          Container(
            width: 200.0,
            height: 180.0,
            padding: EdgeInsets.only(
              bottom: 10.0,
            ),
            child: AllPlatforms(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
          ),
        SizedBox(width: 40.0),
        if (!sizingInformation.isSmallScreen())
          Stack(
            children: [
              Transform.translate(
                offset: Offset(-5.0, -5.0),
                child: Container(
                  width: circleSize,
                  height: circleSize,
                  decoration: BoxDecoration(
                    color: Color(0xff32fffc),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: circleSize,
                    height: circleSize,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(45),
                      border: Border.all(
                        color: Colors.black,
                        width: STROKE_WIDTH,
                      ),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Container(
                    width: circleSize,
                    height: circleSize,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(45),
                      border: Border.all(
                        color: Colors.black,
                        width: STROKE_WIDTH,
                      ),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Container(
                    width: circleSize,
                    height: circleSize,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(45),
                      border: Border.all(
                        color: Colors.black,
                        width: STROKE_WIDTH,
                      ),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Container(
                    width: circleSize,
                    height: circleSize,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(45),
                      border: Border.all(
                        color: Colors.black,
                        width: STROKE_WIDTH,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        Spacer(),
        Column(
          children: [
            Container(
              width: localWidgetWidth / 4,
              height: 53,
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide(
                    color: Colors.black,
                    width: STROKE_WIDTH,
                  ),
                  top: BorderSide(
                    color: Colors.black,
                    width: STROKE_WIDTH,
                  ),
                  bottom: BorderSide(
                    color: Colors.black,
                    width: STROKE_WIDTH,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 22,
            ),
            Container(
              width: localWidgetWidth / 4,
              height: 126,
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide(
                    color: Colors.black,
                    width: STROKE_WIDTH,
                  ),
                  top: BorderSide(
                    color: Colors.black,
                    width: STROKE_WIDTH,
                  ),
                  bottom: BorderSide(
                    color: Colors.black,
                    width: STROKE_WIDTH,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildDeskTopWhatWeDo(
    BuildContext context,
    SizingInformation sizingInformation,
  ) {
    double screenHeight = sizingInformation.screenSize.height;

    return Container(
      width: MAX_WIDTH,
      child: Column(
        children: [
          _desktopRow1(context, sizingInformation),
          SizedBox(height: screenHeight / 20),
          _desktopRow2(context, sizingInformation),
          SizedBox(height: screenHeight / 20),
          _desktopRow3(context, sizingInformation),
        ],
      ),
    );
  }

  Widget _buildMobileWhatWeDo(
    BuildContext context,
    SizingInformation sizingInformation,
  ) {
    var localWidgetWidth = sizingInformation.localWidgetSize.width;
    var circleSize = 18.0;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 30),
        Container(
          height: 90,
          padding: EdgeInsets.symmetric(horizontal: 55),
          child: _buildTitle(context, 35),
        ),
        SizedBox(height: 30),
        Container(
          alignment: Alignment.topLeft,
          padding: EdgeInsets.symmetric(horizontal: 55),
          child: _buildWhatWeDoMessage(context, 15),
        ),
        SizedBox(height: 30),
        Container(
          height: 80,
          padding: EdgeInsets.symmetric(horizontal: 55),
          child: Row(
            children: [
              AnimatedButton(
                width: 160,
                height: 45,
                borderSize: STROKE_WIDTH,
                text: S.of(context).see_projects,
                textSize: 14,
                onPressed: () => onPressed.call(),
              ),
              Spacer(),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Transform.translate(
                    offset: const Offset(-6.0, 15.0),
                    child: Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: new BoxDecoration(
                        color: const Color(0xff32fffc),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                  Container(
                    width: circleSize,
                    height: circleSize,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(45),
                      border: Border.all(
                        color: Colors.black,
                        width: STROKE_WIDTH,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    width: circleSize,
                    height: circleSize,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(45),
                      border: Border.all(
                        color: Colors.black,
                        width: STROKE_WIDTH,
                      ),
                    ),
                  ),
                ],
              ),
              Spacer(),
            ],
          ),
        ),
        SizedBox(height: 35),
        Container(
          padding: EdgeInsets.only(left: 50),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              AnimatedPlaceHolder(
                width: localWidgetWidth / 3,
                height: 133,
              ),
              Expanded(
                child: Transform.translate(
                  offset: Offset(30.0, 0.0),
                  child: CustomPlaceHolder(
                    color: Color(0xFF000000),
                    shadowColor: Colors.grey,
                    fallbackHeight: 133.0,
                    strokeWidth: STROKE_WIDTH,
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Transform.translate(
                offset: Offset(-50.0, 0.0),
                child: CustomPlaceHolder(
                  color: Color(0xFF000000),
                  shadowColor: Colors.grey,
                  fallbackWidth: localWidgetWidth / 3 + 100,
                  fallbackHeight: 150.0,
                  strokeWidth: STROKE_WIDTH,
                ),
              ),
              Container(
                width: 180.0,
                height: 150.0,
                child: ServicesAndProducts(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        if (sizingInformation.isSmallScreen()) {
          return _buildMobileWhatWeDo(context, sizingInformation);
        }
        return _buildDeskTopWhatWeDo(context, sizingInformation);
      },
    );
  }
}
