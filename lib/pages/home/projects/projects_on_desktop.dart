import 'package:fluttech/application.dart';
import 'package:fluttech/model/project_details.dart';
import 'package:fluttech/pages/home/projects/project.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/animated_align_to.dart';
import 'package:fluttech/widgets/background/background.dart';
import 'package:fluttech/pages/home/projects/project_details_dialog.dart';
import 'package:fluttech/widgets/side_bar.dart';
import 'package:flutter/material.dart';

class ProjectsHighLightsOnDesktop extends StatefulWidget {
  final List<ProjectDetails> projects;
  final double contentWidth;

  const ProjectsHighLightsOnDesktop({
    Key? key,
    required this.projects,
    required this.contentWidth,
  }) : super(key: key);

  @override
  _ProjectsHighLightsOnDesktopState createState() =>
      _ProjectsHighLightsOnDesktopState();
}

class _ProjectsHighLightsOnDesktopState
    extends State<ProjectsHighLightsOnDesktop> {
  late bool _clientProjectsGroupHovered;
  late bool _insideProjectsGroupHovered;
  late bool _clientProjectsAligned;
  late bool _insideProjectsAligned;

  @override
  void initState() {
    super.initState();
    _clientProjectsGroupHovered = false;
    _insideProjectsGroupHovered = false;
    _clientProjectsAligned = false;
    _insideProjectsAligned = false;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(AssetImage('assets/images/binaris_screen.png'), context);
    precacheImage(AssetImage('assets/images/binaris_screen_mini.png'), context);
    precacheImage(AssetImage('assets/images/ozonvicc_screen.png'), context);
    precacheImage(AssetImage('assets/images/ozonvicc_screen_mini.png'), context);
    precacheImage(AssetImage('assets/images/hashi_screen.png'), context);
    precacheImage(AssetImage('assets/images/hashi_screen_mini.png'), context);
    precacheImage(AssetImage('assets/images/hyll_screen.png'), context);
    precacheImage(AssetImage('assets/images/hyll_screen_mini.png'), context);
    precacheImage(AssetImage('assets/images/deescus_screen.png'), context);
    precacheImage(AssetImage('assets/images/deescus_screen_mini.png'), context);
    precacheImage(AssetImage('assets/images/evenito_screen.png'), context);
    precacheImage(AssetImage('assets/images/evenito_screen_mini.png'), context);
  }

  @override
  Widget build(BuildContext context) {
    return Background(
      color: Color(0xFFEDEDED),
      blendMode: BlendMode.multiply,
      height: 600,
      child: Container(
        height: 600,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              width: LEFT_PADDING,
              child: SideBar(
                text: S.of(context).projects_highlights,
                activeIndex: 0,
              ),
            ),
            Expanded(
              child: _buildStack(),
            ),
            SizedBox(width: BACKGROUND_MARGIN),
          ],
        ),
      ),
    );
  }

  Widget _buildStack() {
    var secondHalfWidth = widget.contentWidth / 2;
    var firstHalfWidth = widget.contentWidth / 2;
    if (_clientProjectsGroupHovered) {
      secondHalfWidth = widget.contentWidth / 4;
      firstHalfWidth = widget.contentWidth - secondHalfWidth;
    }
    if (_insideProjectsGroupHovered) {
      firstHalfWidth = widget.contentWidth / 4;
      secondHalfWidth = widget.contentWidth - firstHalfWidth;
    }

    return MouseRegion(
      onEnter: (_) {
        FocusScope.of(context).unfocus();
      },
      onExit: (_) {
        setState(() {
          _clientProjectsGroupHovered = false;
          _insideProjectsGroupHovered = false;
        });
      },
      child: Center(
        child: Container(
          width: widget.contentWidth,
          child: Stack(
            children: [
              AnimatedAlignWidget(
                initialAlignment: Alignment(-1.0, -0.9),
                finalAlignment: Alignment(-1.0, -0.9),
                doAnimation:
                    _clientProjectsGroupHovered || _insideProjectsGroupHovered,
                child: Project(
                  width: 200,
                  height: 370,
                  client: widget.projects[1].clientName,
                  photoUrl: 'assets/images/hyll_screen_mini.png',
                  projectName: widget.projects[1].title,
                  technologies: 'Flutter, Django, Python',
                  platforms: 'iOS, Android',
                  animated: _clientProjectsAligned,
                  onTap: () {
                    showProjectDetailsDialog(
                      context,
                      widget.projects[1],
                      widget.contentWidth,
                    );
                  },
                ),
              ),
              AnimatedAlignWidget(
                initialAlignment: Alignment(-0.7, -0.3),
                finalAlignment: _clientProjectsGroupHovered
                    ? Alignment(-0.4, -0.9)
                    : _insideProjectsGroupHovered
                        ? Alignment(-1.0, -0.3)
                        : Alignment(-0.7, -0.3),
                doAnimation:
                    _clientProjectsGroupHovered || _insideProjectsGroupHovered,
                child: Project(
                  width: 200,
                  height: 370,
                  client: widget.projects[0].clientNameShort,
                  photoUrl: 'assets/images/deescus_screen_mini.png',
                  projectName: widget.projects[0].title,
                  technologies: 'Flutter, Firebase',
                  platforms: 'iOS, Android, MacOs, Web',
                  animated: _clientProjectsAligned,
                  onTap: () {
                    showProjectDetailsDialog(
                      context,
                      widget.projects[0],
                      widget.contentWidth,
                    );
                  },
                ),
              ),
              AnimatedAlignWidget(
                initialAlignment: Alignment(-0.4, 0.3),
                finalAlignment: _clientProjectsGroupHovered
                    ? Alignment(0.2, -0.9)
                    : _insideProjectsGroupHovered
                        ? Alignment(-1.0, 0.3)
                        : Alignment(-0.4, 0.3),
                doAnimation:
                    _clientProjectsGroupHovered || _insideProjectsGroupHovered,
                onEnd: () {
                  setState(() {
                    if (_clientProjectsGroupHovered) {
                      _clientProjectsAligned = true;
                      _insideProjectsAligned = false;
                    } else {
                      _clientProjectsAligned = false;
                    }
                  });
                },
                child: Project(
                  width: 200,
                  height: 370,
                  client: widget.projects[2].clientName,
                  photoUrl: 'assets/images/evenito_screen_mini.png',
                  projectName: widget.projects[2].title,
                  technologies: 'Flutter',
                  platforms: 'iOS, Android',
                  animated: _clientProjectsAligned,
                  onTap: () {
                    showProjectDetailsDialog(
                      context,
                      widget.projects[2],
                      widget.contentWidth,
                    );
                  },
                ),
              ),
              AnimatedAlignWidget(
                initialAlignment: Alignment(0.3, -0.9),
                finalAlignment: _insideProjectsGroupHovered
                    ? Alignment(-0.3, -0.9)
                    : _clientProjectsGroupHovered
                        ? Alignment(1.0, -0.9)
                        : Alignment(0.3, -0.9),
                doAnimation:
                    _clientProjectsGroupHovered || _insideProjectsGroupHovered,
                child: Project(
                  width: 200,
                  height: 370,
                  photoUrl: 'assets/images/hashi_screen_mini.png',
                  projectName: widget.projects[5].title,
                  technologies: 'Flutter',
                  platforms: 'Android',
                  animated: _insideProjectsAligned,
                  onTap: () {
                    showProjectDetailsDialog(
                      context,
                      widget.projects[5],
                      widget.contentWidth,
                    );
                  },
                ),
              ),
              AnimatedAlignWidget(
                initialAlignment: Alignment(0.6, -0.3),
                finalAlignment: _insideProjectsGroupHovered
                    ? Alignment(0.3, -0.9)
                    : _clientProjectsGroupHovered
                        ? Alignment(1.0, -0.3)
                        : Alignment(0.6, -0.3),
                doAnimation:
                    _clientProjectsGroupHovered || _insideProjectsGroupHovered,
                child: Project(
                  width: 200,
                  height: 370,
                  photoUrl: 'assets/images/ozonvicc_screen_mini.png',
                  projectName: widget.projects[4].title,
                  technologies: 'Flutter',
                  platforms: 'iOS, Android',
                  animated: _insideProjectsAligned,
                  onTap: () {
                    showProjectDetailsDialog(
                      context,
                      widget.projects[4],
                      widget.contentWidth,
                    );
                  },
                ),
              ),
              AnimatedAlignWidget(
                initialAlignment: Alignment(0.9, 0.3),
                finalAlignment: _insideProjectsGroupHovered
                    ? Alignment(0.9, -0.9)
                    : _clientProjectsGroupHovered
                        ? Alignment(1.0, 0.3)
                        : Alignment(0.9, 0.3),
                doAnimation:
                    _clientProjectsGroupHovered || _insideProjectsGroupHovered,
                onEnd: () {
                  setState(() {
                    if (_insideProjectsGroupHovered) {
                      _insideProjectsAligned = true;
                      _clientProjectsAligned = false;
                    } else {
                      _insideProjectsAligned = true;
                    }
                  });
                },
                child: Project(
                  width: 200,
                  height: 370,
                  photoUrl: 'assets/images/binaris_screen_mini.png',
                  projectName: widget.projects[3].title,
                  technologies: 'Flutter',
                  platforms: 'iOS, Android',
                  animated: _insideProjectsAligned,
                  onTap: () {
                    showProjectDetailsDialog(
                      context,
                      widget.projects[3],
                      widget.contentWidth,
                    );
                  },
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  color: Colors.black,
                  width: widget.contentWidth / 2 - 100,
                  height: STROKE_WIDTH,
                  padding: EdgeInsets.zero,
                ),
              ),
              Positioned(
                top: 0,
                right: 0,
                child: Container(
                  color: Colors.black,
                  width: widget.contentWidth / 2 + 50,
                  height: STROKE_WIDTH,
                  padding: EdgeInsets.zero,
                ),
              ),
              IgnorePointer(
                ignoring: _clientProjectsGroupHovered && _clientProjectsAligned,
                child: MouseRegion(
                  onEnter: (_) {
                    setState(() {
                      _clientProjectsGroupHovered = true;
                      _insideProjectsGroupHovered = false;
                    });
                  },
                  child: Container(
                    width: firstHalfWidth,
                  ),
                ),
              ),
              IgnorePointer(
                ignoring: _insideProjectsGroupHovered && _insideProjectsAligned,
                child: Align(
                  alignment: Alignment.centerRight,
                  child: MouseRegion(
                    onEnter: (details) {
                      setState(() {
                        _insideProjectsGroupHovered = true;
                        _clientProjectsGroupHovered = false;
                      });
                    },
                    child: Container(
                      width: secondHalfWidth,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
