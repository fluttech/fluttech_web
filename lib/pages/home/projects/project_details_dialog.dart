import 'package:fluttech/model/project_details.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/animated_text.dart';
import 'package:fluttech/widgets/link.dart';
import 'package:fluttech/widgets/store_button.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:url_launcher/url_launcher.dart';

class ProjectDetailsView extends StatelessWidget {
  static const dialogName = 'ProjectDetails';
  final ProjectDetails details;
  final double? maxWidth;
  final bool mobileVersion;

  const ProjectDetailsView({
    Key? key,
    required this.details,
    this.maxWidth,
    this.mobileVersion = false,
  }) : super(key: key);

  Widget _buildTitle(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: SelectableText(
                details.title,
                style: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            CustomLink(
              url: details.webSiteUrl,
              isMobileView: mobileVersion,
              child: PhysicalModel(
                color: Colors.white.withOpacity(.5),
                elevation: 10.0,
                borderRadius: BorderRadius.circular(10),
                child: InkWell(
                  onTap: () {
                    launch(details.webSiteUrl);
                  },
                  child: Container(
                    width: 70,
                    height: 70,
                    color: Colors.transparent,
                    child: Image.asset(
                      details.logoUrl,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 10),
        if (details.clientName != null)
          CustomLink(
            url: details.clientWebSite,
            isMobileView: mobileVersion,
            child: AnimatedText(
              text: details.clientName!,
              textSize: 19,
              onPressed: () => launch(details.clientWebSite),
            ),
          ),
      ],
    );
  }

  Widget _buildContent(BuildContext context) {
    return SelectableText(
      details.description,
      style: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w400,
      ),
      maxLines: 16,
    );
  }

  Widget _buildReview(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SelectableText(
          'User review',
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w700,
          ),
        ),
        SizedBox(height: 10),
        SelectableText(
          details.review ?? '',
          style: TextStyle(
            fontSize: 14,
            height: 1.5,
          ),
        ),
      ],
    );
  }

  Widget _buildStoreButtons(BuildContext context, bool isMobile) {
    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      spacing: 15,
      runSpacing: 15,
      children: [
        if (details.appleStoreUrl != null)
          StoreButton(
            image: 'assets/images/appstore-apple.svg',
            height: isMobile ? 35 : 44,
            url: details.appleStoreUrl!,
          ),
        if (details.playStoreUrl != null)
          StoreButton(
            image: 'assets/images/appstore-android.svg',
            height: isMobile ? 35 : 44,
            url: details.playStoreUrl!,
          ),
      ],
    );
  }

  Widget _buildDesktopProject(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: maxWidth,
          height: 600,
          margin: EdgeInsets.all(10.0),
          child: ResponsiveBuilder(
            builder: (context, sizingInformation) {
              if (sizingInformation.isMobileView(context)) {
                WidgetsBinding.instance!.addPostFrameCallback((_) {
                  Navigator.of(context).pop();
                });
              }
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Hero(
                    tag: details.title,
                    child: Image.asset(
                      details.screenUrl,
                    ),
                  ),
                  SizedBox(width: 20),
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 25.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _buildTitle(context),
                          SizedBox(height: 30),
                          _buildContent(context),
                          if (details.review != null) _buildReview(context),
                          Spacer(),
                          _buildStoreButtons(
                            context,
                            sizingInformation.isMobileView(context),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
        Positioned(
          right: -10,
          top: -10,
          child: IconButton(
            iconSize: 20,
            padding: EdgeInsets.zero,
            hoverColor: Colors.transparent,
            highlightColor: Colors.transparent,
            icon: const Icon(Icons.close),
            tooltip: MaterialLocalizations.of(context).closeButtonTooltip,
            onPressed: () {
              Navigator.maybePop(context);
            },
          ),
        ),
      ],
    );
  }

  Widget _buildMobileProject(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildTitle(context),
            SizedBox(height: 30),
            _buildContent(context),
            SizedBox(height: 10),
            if (details.review != null) _buildReview(context),
          ],
        ),
        Spacer(),
        _buildStoreButtons(context, true),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if (mobileVersion) {
      return _buildMobileProject(context);
    }
    return _buildDesktopProject(context);
  }
}

void showProjectDetailsDialog(
  BuildContext context,
  ProjectDetails details,
  double contentWidth,
) async {
  Navigator.push(
    context,
    PageRouteBuilder(
      barrierDismissible: true,
      barrierColor: Colors.transparent,
      opaque: false,
      transitionDuration: Duration(milliseconds: 700),
      reverseTransitionDuration: Duration(milliseconds: 700),
      transitionsBuilder: (context, anim1, anim2, child) {
        return FadeTransition(
          opacity: anim1,
          child: child,
        );
      },
      pageBuilder: (context, anim1, anim2) => Center(
        child: AlertDialog(
          contentPadding: EdgeInsets.all(10.0),
          scrollable: true,
          content: ProjectDetailsView(
            details: details,
            maxWidth: contentWidth,
          ),
        ),
      ),
    ),
  );
}
