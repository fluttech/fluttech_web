import 'dart:math';
import 'package:fluttech/model/project_details.dart';
import 'package:fluttech/pages/home/projects/projects_on_desktop.dart';
import 'package:fluttech/pages/home/projects/projetcts_on_mobile.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ProjectsHighlights extends StatelessWidget {
  final List<ProjectDetails> projects;

  const ProjectsHighlights({
    Key? key,
    required this.projects,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        if (sizingInformation.isMobileView(context)) {
          return ProjectsHighLightsOnMobile(
            projects: projects,
          );
        }
        var localWidgetWidth = sizingInformation.localWidgetSize.width;
        var contentWidth = min(localWidgetWidth, MAX_WIDTH) -
            (LEFT_PADDING + BACKGROUND_MARGIN);
        return ProjectsHighLightsOnDesktop(
          projects: projects,
          contentWidth: contentWidth,
        );
      },
    );
  }
}