import 'package:fluttech/model/project_details.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/background/background.dart';
import 'package:fluttech/widgets/circle_page_indicator.dart';
import 'package:fluttech/pages/home/projects/project_details_dialog.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ProjectsHighLightsOnMobile extends StatefulWidget {
  final List<ProjectDetails> projects;

  const ProjectsHighLightsOnMobile({
    Key? key,
    required this.projects,
  }) : super(key: key);

  @override
  _ProjectsHighLightsOnMobileState createState() =>
      _ProjectsHighLightsOnMobileState();
}

class _ProjectsHighLightsOnMobileState
    extends State<ProjectsHighLightsOnMobile> {
  final _currentPageNotifier = ValueNotifier<int>(0);
  PageController _controller = PageController(
    initialPage: 0,
    viewportFraction: 1.0,
  );
  List<ProjectDetails> _projects = [];

  @override
  void initState() {
    super.initState();
    _projects = widget.projects;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _buildCircleIndicator() {
    return Positioned(
      left: 0.0,
      right: 0.0,
      bottom: 0.0,
      child: Padding(
        padding: EdgeInsets.all(12.0),
        child: CirclePageIndicator(
          itemCount: 6,
          selectedDotColor: Colors.black,
          dotColor: Color(0xFFBFBFBF),
          currentPageNotifier: _currentPageNotifier,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        return Background(
          color: Color(0xFFEDEDED),
          blendMode: BlendMode.multiply,
          height: 700,
          child: Container(
            height: 700,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  width: BACKGROUND_MARGIN / 2,
                  alignment: Alignment.center,
                  child: RotatedBox(
                    quarterTurns: -1,
                    child: Text(
                      'PROJECT HIGHLIGHTS',
                      style:
                          TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                    ),
                  ),
                ),
                Stack(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(bottom: 32.0),
                      child: Container(
                        width: sizingInformation.screenSize.width -
                            BACKGROUND_MARGIN,
                        child: PageView(
                          controller: _controller,
                          onPageChanged: (int index) {
                            _currentPageNotifier.value = index;
                          },
                          children: [
                            Padding(
                              padding: EdgeInsets.all(20.0),
                              child: ProjectDetailsView(
                                details: _projects[1],
                                mobileVersion: true,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(20.0),
                              child: ProjectDetailsView(
                                details: _projects[0],
                                mobileVersion: true,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(20.0),
                              child: ProjectDetailsView(
                                details: _projects[2],
                                mobileVersion: true,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(20.0),
                              child: ProjectDetailsView(
                                details: _projects[3],
                                mobileVersion: true,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(20.0),
                              child: ProjectDetailsView(
                                details: _projects[4],
                                mobileVersion: true,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(20.0),
                              child: ProjectDetailsView(
                                details: _projects[5],
                                mobileVersion: true,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    _buildCircleIndicator(),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
