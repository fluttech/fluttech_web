import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/custom_placeholder.dart';
import 'package:flutter/material.dart';

class Project extends StatefulWidget {
  final double width;
  final double height;
  final double strokeWidth;
  final String projectName;
  final String? client;
  final String technologies;
  final String platforms;
  final String photoUrl;
  final bool animated;
  final VoidCallback onTap;

  const Project({
    Key? key,
    required this.width,
    required this.height,
    required this.projectName,
    required this.technologies,
    required this.platforms,
    required this.photoUrl,
    required this.onTap,
    this.client,
    this.animated = false,
    this.strokeWidth = STROKE_WIDTH,
  }) : super(key: key);

  @override
  _ProjectState createState() => _ProjectState();
}

class _ProjectState extends State<Project> {
  bool _mouseHover = false;

  Widget _buildEmptyProject() {
    var nameDistance = widget.height / 15;
    return Container(
      width: widget.width,
      height: widget.height,
      child: Stack(
        children: [
          CustomPlaceHolder(
            color: Color(0xFF000000),
            shadowColor: Colors.grey,
            fallbackWidth: widget.width,
            fallbackHeight: widget.height,
            strokeWidth: widget.strokeWidth,
          ),
          Positioned(
            left: 0,
            right: 0,
            top: nameDistance,
            child: Text(
              widget.projectName,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    fontWeight: FontWeight.w700,
                    fontSize: 20,
                  ),
            ),
          ),
          Positioned(
            left: 10,
            right: 10,
            bottom: 35,
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(
                horizontal: 8,
                vertical: 5,
              ),
              color: Color(0xFFEEF2F5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (widget.client != null) ...[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Text(
                        //   'Client: ',
                        //   style: Theme.of(context).textTheme.headline6.copyWith(
                        //         fontSize: 12,
                        //       ),
                        // ),
                        Text(
                          widget.client ?? '',
                          style: Theme.of(context).textTheme.headline6!.copyWith(
                                fontSize: 13,
                                fontWeight: FontWeight.w700,
                              ),
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                  ],
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Text(
                      //   'Tech: ',
                      //   style: Theme.of(context).textTheme.headline6.copyWith(
                      //         fontSize: 12,
                      //       ),
                      // ),
                      Flexible(
                        child: Text(
                          widget.technologies,
                          style: Theme.of(context).textTheme.headline6!.copyWith(
                                fontSize: 12,
                                fontWeight: FontWeight.w700,
                              ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Text(
                      //   'Platforms: ',
                      //   style: Theme.of(context).textTheme.headline6.copyWith(
                      //     fontSize: 12,
                      //   ),
                      // ),
                      Flexible(
                        child: Text(
                          widget.platforms,
                          style: Theme.of(context).textTheme.headline6!.copyWith(
                                fontSize: 12,
                                fontWeight: FontWeight.w700,
                              ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildProject() {
    return Hero(
      tag: widget.projectName,
      child: AnimatedSwitcher(
        duration: Duration(milliseconds: 300),
        reverseDuration: Duration(milliseconds: 100),
        child: widget.animated && _mouseHover
            ? Container(
                key: ValueKey(widget.projectName),
                width: widget.width,
                height: widget.height,
                child: Image.asset(
                  widget.photoUrl,
                  fit: BoxFit.fill,
                ),
              )
            : _buildEmptyProject(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onHover: (value) {
          setState(() {
            _mouseHover = value;
          });
        },
        onTapDown: (_) {
          setState(() {
            _mouseHover = true;
          });
        },
        onTapCancel: () {
          setState(() {
            _mouseHover = false;
          });
        },
        onTap: widget.onTap,
        child: _buildProject(),
      ),
    );
  }
}
