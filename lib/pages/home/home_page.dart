import 'package:fluttech/pages/home/home_screen.dart';
import 'package:flutter/material.dart';

class HomePage extends Page {
  static const String route = '/';

  final Widget navBar;

  HomePage({required this.navBar}) : super(key: ValueKey('HomePage'));

  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
      opaque: false,
      transitionDuration: Duration(milliseconds: 200),
      transitionsBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child,
      ) {
        return FadeTransition(
          opacity: animation,
          child: child,
        );
      },
      settings: this,
      pageBuilder: (context, animation, animation2) {
        return HomeScreen(
          navBar: navBar,
        );
      },
    );
  }
}
