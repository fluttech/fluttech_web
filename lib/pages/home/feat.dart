import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/background/background.dart';
import 'package:fluttech/widgets/side_bar.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class Feat extends StatelessWidget {
  Widget _buildTitle(double size) {
    return Text(
      'Our work is featured in:',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: size,
        color: Color(0xFFCB4CFD),
        decoration: TextDecoration.underline,
      ),
    );
  }

  Widget _buildDeskTopFeat(SizingInformation sizingInformation) {
    var localWidgetWidth = sizingInformation.localWidgetSize.width;
    var contentWidth = localWidgetWidth - (LEFT_PADDING + BACKGROUND_MARGIN);
    return Background(
      color: Color(0xFFEDEDED),
      blendMode: BlendMode.multiply,
      height: 400,
      child: Container(
        height: 400,
        color: Colors.transparent,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              width: LEFT_PADDING,
              child: SideBar(
                text: 'FEAT',
                activeIndex: 2,
              ),
            ),
            Flexible(
              child: Container(
                width: contentWidth,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _buildTitle(45),
                    Image.asset(
                      'assets/images/logo_critbiz.png',
                      color: Colors.black,
                    ),
                    Container(
                      color: Color(0xFFCB4CFD),
                      width: 200,
                      height: 6,
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              width: BACKGROUND_MARGIN,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMobileFeat(SizingInformation sizingInformation) {
    return Background(
      color: Color(0xFFEDEDED),
      blendMode: BlendMode.multiply,
      height: 350,
      child: Container(
        height: 350,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              width: BACKGROUND_MARGIN / 2,
              alignment: Alignment.center,
              child: RotatedBox(
                quarterTurns: -1,
                child: Text(
                  'FEAT',
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildTitle(28),
                  SizedBox(
                    width: sizingInformation.localWidgetSize.width / 2,
                    child: Image.asset(
                      'assets/images/logo_critbiz.png',
                      color: Colors.black,
                    ),
                  ),
                  Container(
                    color: Color(0xFFCB4CFD),
                    width: 150,
                    height: 6,
                  ),
                ],
              ),
            ),
            SizedBox(
              width: BACKGROUND_MARGIN / 2,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        if (sizingInformation.isSmallScreen()) {
          return _buildMobileFeat(sizingInformation);
        }
        return _buildDeskTopFeat(sizingInformation);
      },
    );
  }
}
