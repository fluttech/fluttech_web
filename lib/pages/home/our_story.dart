import 'package:fluttech/application.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/circle_page_indicator.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class OurStory extends StatefulWidget {
  const OurStory({
    Key? key,
  }) : super(key: key);

  @override
  _OurStoryState createState() => _OurStoryState();
}

class _OurStoryState extends State<OurStory> {
  int selectedIndex = 0;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(AssetImage('assets/images/brothers1.png'), context);
    precacheImage(AssetImage('assets/images/brothers2.png'), context);
    precacheImage(AssetImage('assets/images/brothers3.png'), context);
  }

  Widget _buildDeskTopOurStoryContent(SizingInformation sizingInformation) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: 350,
              height: 120,
              alignment: Alignment.centerLeft,
              child: SelectableText(
                S.of(context).our_story,
                style: TextStyle(
                  fontSize: 45,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            Container(
              width: 400,
              height: 250,
              alignment: Alignment.centerLeft,
              child: SelectableText(
                S.of(context).our_story_description,
                style: TextStyle(
                  fontSize: 18,
                  height: 1.5,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ],
        ),
        SizedBox(width: 50),
        if (sizingInformation.localWidgetSize.width >= 860)
          Expanded(
            child: Container(
              width: 400,
              height: 450,
              child: Stack(
                children: <Widget>[
                  IgnorePointer(
                    ignoring: selectedIndex != 0 && selectedIndex != 2,
                    child: AnimatedOpacity(
                      opacity:
                          selectedIndex == 0 || selectedIndex == 2 ? 1.0 : 0.0,
                      duration: Duration(milliseconds: 400),
                      child: AnimatedStoryItem(
                        index: 2,
                        width: 190,
                        height: 250,
                        maxWidth: 340,
                        maxHeight: 420,
                        photoUrl: 'assets/images/brothers3.png',
                        strokeWidth: STROKE_WIDTH,
                        alignment: Alignment(-0.6, -0.6),
                        onTap: (value) {
                          setState(() {
                            selectedIndex = value;
                          });
                        },
                      ),
                    ),
                  ),
                  IgnorePointer(
                    ignoring: selectedIndex != 0 && selectedIndex != 1,
                    child: AnimatedOpacity(
                      opacity:
                          selectedIndex == 0 || selectedIndex == 1 ? 1.0 : 0.0,
                      duration: Duration(milliseconds: 400),
                      child: AnimatedStoryItem(
                        index: 1,
                        width: 190,
                        height: 250,
                        maxWidth: 320,
                        maxHeight: 420,
                        photoUrl: 'assets/images/brothers1.png',
                        strokeWidth: STROKE_WIDTH,
                        alignment: Alignment(0.3, 0.0),
                        onTap: (value) {
                          setState(() {
                            selectedIndex = value;
                          });
                        },
                      ),
                    ),
                  ),
                  IgnorePointer(
                    ignoring: selectedIndex != 0 && selectedIndex != 3,
                    child: AnimatedOpacity(
                      opacity:
                          selectedIndex == 0 || selectedIndex == 3 ? 1.0 : 0.0,
                      duration: Duration(milliseconds: 400),
                      child: AnimatedStoryItem(
                        index: 3,
                        width: 190,
                        height: 250,
                        maxWidth: 310,
                        maxHeight: 420,
                        photoUrl: 'assets/images/brothers2.png',
                        strokeWidth: STROKE_WIDTH,
                        alignment: Alignment(-0.2, 0.6),
                        onTap: (value) {
                          setState(() {
                            selectedIndex = value;
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }

  Widget _buildDeskTopOurStory(SizingInformation sizingInformation) {
    return Container(
      width: MAX_WIDTH,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: LEFT_PADDING,
          ),
          Flexible(
            child: _buildDeskTopOurStoryContent(sizingInformation),
          ),
          SizedBox(
            width: RIGHT_PADDING,
          ),
        ],
      ),
    );
  }

  Widget _buildMobileWhatWeDo() {
    return OurStoryOnMobile();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        if (sizingInformation.isSmallScreen()) {
          return _buildMobileWhatWeDo();
        }
        return _buildDeskTopOurStory(sizingInformation);
      },
    );
  }
}

class StoryItem extends StatelessWidget {
  final double strokeWidth;
  final VoidCallback selectIndex;

  const StoryItem({
    Key? key,
    required this.strokeWidth,
    required this.selectIndex,
  }) : super(key: key);

  Widget _buildEmptyItem() {
    return Container(
      key: UniqueKey(),
      width: 220,
      height: 270,
      child: Stack(
        children: [
          Container(
            width: 200,
            height: 250,
            color: Colors.white,
            child: Placeholder(
              // color: const Color(0xFF000000),
              strokeWidth: STROKE_WIDTH,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: selectIndex,
      child: _buildEmptyItem(),
    );
  }
}

class AnimatedStoryItem extends StatefulWidget {
  final int index;
  final double width;
  final double height;
  final double maxWidth;
  final double maxHeight;
  final String photoUrl;
  final double strokeWidth;
  final Alignment alignment;
  final ValueChanged<int> onTap;

  const AnimatedStoryItem({
    Key? key,
    required this.index,
    required this.width,
    required this.height,
    required this.maxWidth,
    required this.maxHeight,
    required this.photoUrl,
    required this.strokeWidth,
    required this.alignment,
    required this.onTap,
  }) : super(key: key);

  @override
  _AnimatedStoryItemState createState() => _AnimatedStoryItemState();
}

class _AnimatedStoryItemState extends State<AnimatedStoryItem> {
  Alignment _masterAlignment = Alignment.center;
  late Alignment _alignment;
  late double _animatedHeight;
  late double _animatedWidth;
  bool _isAnimated = false;
  bool _mouseHover = false;

  @override
  void initState() {
    super.initState();
    _alignment = widget.alignment;
    _animatedWidth = widget.width;
    _animatedHeight = widget.height;
  }

  Widget _buildChild() {
    if (!_mouseHover && _animatedWidth != widget.maxWidth) {
      return PhysicalModel(
        color: Colors.black,
        shadowColor: Colors.purple,
        elevation: 12.0,
        child: Container(
          color: Colors.white,
          child: Placeholder(
            color: Color(0xFF000000),
            strokeWidth: STROKE_WIDTH,
          ),
        ),
      );
    } else {
      return StoryImage(
        photoUrl: widget.photoUrl,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(seconds: 1),
      curve: Curves.fastOutSlowIn,
      alignment: _alignment,
      child: AnimatedContainer(
        duration: Duration(seconds: 1),
        curve: Curves.fastOutSlowIn,
        width: _animatedWidth,
        height: _animatedHeight,
        child: MouseRegion(
          onEnter: (event) {
            setState(() {
              _mouseHover = true;
            });
          },
          onExit: (event) {
            setState(() {
              _mouseHover = false;
            });
          },
          child: InkWell(
            onTap: () {
              widget
                  .onTap(_animatedWidth == widget.maxWidth ? 0 : widget.index);
              setState(() {
                _animatedWidth == widget.maxWidth
                    ? _isAnimated = true
                    : _isAnimated = false;

                _isAnimated
                    ? _alignment = widget.alignment
                    : _alignment = _masterAlignment;

                _isAnimated
                    ? _animatedWidth = widget.width
                    : _animatedWidth = widget.maxWidth;

                _isAnimated
                    ? _animatedHeight = widget.height
                    : _animatedHeight = widget.maxHeight;
              });
            },
            child: AnimatedSwitcher(
              duration: Duration(milliseconds: 300),
              reverseDuration: Duration(milliseconds: 100),
              child: _buildChild(),
            ),
          ),
        ),
      ),
    );
  }
}

class StoryImage extends StatelessWidget {
  final double? width;
  final double? height;
  final String photoUrl;

  const StoryImage({
    Key? key,
    required this.photoUrl,
    this.width,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black45,
              blurRadius: 10.0,
              spreadRadius: 0.2,
              offset: Offset(
                1.0,
                1.0,
              ),
            )
          ],
        ),
        padding: EdgeInsets.all(8.0),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Image.asset(
                photoUrl,
                fit: BoxFit.contain,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class OurStoryOnMobile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 30),
        Container(
          height: 80,
          padding: EdgeInsets.symmetric(horizontal: 55),
          child: Text(
            S.of(context).our_story,
            style: TextStyle(
              fontSize: 35,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
        SizedBox(height: 20),
        Container(
          alignment: Alignment.topLeft,
          padding: EdgeInsets.symmetric(horizontal: 55),
          child: Text(
            S.of(context).our_story_description,
            style: TextStyle(
              fontSize: 15,
              height: 1.5,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        SizedBox(height: 20),
        CustomPageView(),
      ],
    );
  }
}

class CustomPageView extends StatefulWidget {
  @override
  _CustomPageViewState createState() => _CustomPageViewState();
}

class _CustomPageViewState extends State<CustomPageView> {
  final _currentPageNotifier = ValueNotifier<int>(0);
  PageController _controller = PageController(
    initialPage: 0,
    viewportFraction: 1.0,
  );

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(AssetImage('assets/images/brothers1.png'), context);
    precacheImage(AssetImage('assets/images/brothers2.png'), context);
    precacheImage(AssetImage('assets/images/brothers3.png'), context);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget _buildCircleIndicator() {
    return Positioned(
      left: 0.0,
      right: 0.0,
      bottom: 0.0,
      child: Padding(
        padding: EdgeInsets.all(12.0),
        child: CirclePageIndicator(
          itemCount: 3,
          selectedDotColor: Colors.black,
          dotColor: Color(0xFFBFBFBF),
          currentPageNotifier: _currentPageNotifier,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        var localWith = sizingInformation.localWidgetSize.width;
        var localPadding = (localWith - 350) / 2;
        return Container(
          width: localWith,
          height: MediaQuery.of(context).size.height * 0.5,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(
                width: BACKGROUND_MARGIN / 2,
              ),
              Stack(
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: 32.0),
                    child: Container(
                      width: sizingInformation.screenSize.width -
                          BACKGROUND_MARGIN,
                      child: PageView(
                        controller: _controller,
                        onPageChanged: (int index) {
                          setState(() {
                            _currentPageNotifier.value = index;
                          });
                        },
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: localPadding,
                              vertical: 25.0,
                            ),
                            child: StoryImage(
                              width: 400,
                              height: 450,
                              photoUrl: 'assets/images/brothers2.png',
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: localPadding,
                              vertical: 25.0,
                            ),
                            child: StoryImage(
                              width: 400,
                              height: 450,
                              photoUrl: 'assets/images/brothers3.png',
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: localPadding,
                              vertical: 25.0,
                            ),
                            child: StoryImage(
                              width: 400,
                              height: 450,
                              photoUrl: 'assets/images/brothers1.png',
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  _buildCircleIndicator(),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
