import 'package:cloud_functions/cloud_functions.dart';
import 'package:fluttech/application.dart';
import 'package:fluttech/bloc/cloud_functions/cloud_functions_bloc.dart';
import 'package:fluttech/model/project_details.dart';
import 'package:fluttech/pages/home/contact_us.dart';
import 'package:fluttech/pages/home/home_page.dart';
import 'package:fluttech/pages/home/our_story.dart';
import 'package:fluttech/pages/home/our_team.dart';
import 'package:fluttech/pages/home/projects/projects.dart';
import 'package:fluttech/pages/home/what_we_do.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/background/background.dart';
import 'package:fluttech/widgets/bottom_bar.dart';
import 'package:fluttech/widgets/navbar/navbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

class HomeScreen extends StatefulWidget {
  final Widget navBar;

  HomeScreen({
    Key? key,
    required this.navBar,
  }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late AutoScrollController _controller;
  late NavBar _navbar;
  String _subject = '';

  @override
  void initState() {
    super.initState();
    _controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: Axis.vertical,
    );

    _navbar = NavBar(
      page: HomePage.route,
      onContactPressed: () {
        setState(() {
          _subject = '';
        });
        _controller.scrollToIndex(
          1,
          preferPosition: AutoScrollPosition.begin,
          duration: Duration(milliseconds: 500),
        );
      },
    );
  }

  List<ProjectDetails> _projectDetails(BuildContext context) {
    return [
      ProjectDetails(
        title: S.of(context).deescus_title,
        clientName: S.of(context).deescus_clientName,
        clientNameShort: 'M&P AG',
        clientWebSite: 'https://muehlemann-popp.ch/welcome',
        description: S.of(context).deescus_description,
        logoUrl: 'assets/images/deescus_logo.png',
        screenUrl: 'assets/images/deescus_screen.png',
        appleStoreUrl: 'https://apps.apple.com/app/id1512737904',
        webSiteUrl: 'https://deescus.com',
        playStoreUrl:
            'https://play.google.com/store/apps/details?id=com.deescus.android',
      ),
      ProjectDetails(
        title: S.of(context).hyll_title,
        clientName: S.of(context).hyll_clientName,
        clientWebSite: 'https://hyll.com',
        description: S.of(context).hyll_description,
        logoUrl: 'assets/images/hyll_logo.png',
        screenUrl: 'assets/images/hyll_screen.png',
        appleStoreUrl: 'https://apps.apple.com/ch/app/hyll/id1491010821',
        webSiteUrl: 'https://hyll.com/',
        playStoreUrl:
            'https://play.google.com/store/apps/details?id=com.hyll.android',
      ),
      ProjectDetails(
        title: S.of(context).evenito_title,
        clientName: S.of(context).evenito_clientName,
        clientWebSite: 'https://evenito.com',
        description: S.of(context).evenito_description,
        logoUrl: 'assets/images/evenito_logo.png',
        screenUrl: 'assets/images/evenito_screen.png',
        appleStoreUrl:
            'https://apps.apple.com/ch/app/check-in-guestlist-evenito/id1572822238',
        webSiteUrl: 'https://evenito.com/',
        playStoreUrl: '',
      ),
      ProjectDetails(
        title: S.of(context).binaris_title,
        clientWebSite: '',
        description: S.of(context).binaris_description,
        logoUrl: 'assets/images/binaris_logo.png',
        screenUrl: 'assets/images/binaris_screen.png',
        appleStoreUrl: 'https://apps.apple.com/app/1512633108',
        webSiteUrl: 'https://fluttech.com',
        playStoreUrl:
            'https://play.google.com/store/apps/details?id=com.mjk.rigy.binary',
        review:
            '"Nice simple game that becomes at times more challenging than you expect."',
      ),
      ProjectDetails(
        title: S.of(context).ozonvicc_title,
        clientWebSite: '',
        description: S.of(context).ozonvicc_description,
        logoUrl: 'assets/images/ozonvicc_logo.png',
        screenUrl: 'assets/images/ozonvicc_screen.png',
        appleStoreUrl: 'https://apps.apple.com/app/1479397425',
        webSiteUrl: 'https://ozonvicc.hu',
        playStoreUrl:
            'https://play.google.com/store/apps/details?id=com.mjk.rigy.jokes',
        review:
            '"Great, sensible and humorous. And I think the idea of a daily joke got really good. I can only recommend it to those who want to laugh."',
      ),
      ProjectDetails(
        title: S.of(context).hashi_title,
        clientWebSite: '',
        description: S.of(context).hashi_description,
        logoUrl: 'assets/images/hashi_logo.png',
        screenUrl: 'assets/images/hashi_screen.png',
        webSiteUrl: 'https://fluttech.com',
        playStoreUrl:
            'https://play.google.com/store/apps/details?id=com.mjkmobileapps.shimanohashi',
        review:
            '"It\'s a great little game ! Easy and relaxing way to spend some time."',
      ),
    ];
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  List<Widget> _content(double screenHeight) {
    return [
      SizedBox(height: screenHeight / 15),
      WhatWeDo(
        onPressed: () {
          _controller.scrollToIndex(
            0,
            preferPosition: AutoScrollPosition.begin,
            duration: Duration(milliseconds: 500),
          );
        },
      ),
      SizedBox(height: screenHeight / 20),
      AutoScrollTag(
        key: ValueKey(0),
        controller: _controller,
        index: 0,
        child: ProjectsHighlights(
          projects: _projectDetails(context),
        ),
      ),
      SizedBox(height: screenHeight / 20),
      OurTeam(
        onJobApplied: (text) {
          setState(() {
            _subject = text;
          });
          _controller.scrollToIndex(
            1,
            preferPosition: AutoScrollPosition.begin,
            duration: Duration(milliseconds: 500),
          );
        },
      ),
      SizedBox(height: screenHeight / 20),
      OurStory(),
      SizedBox(height: screenHeight / 20),
      AutoScrollTag(
        key: ValueKey(1),
        controller: _controller,
        index: 1,
        child: QuoteSection(
          subject: _subject,
        ),
      ),
      BottomBar(),
    ];
  }

  Widget _buildScreen(SizingInformation sizingInformation) {
    double screenHeight = sizingInformation.screenSize.height;
    return Scrollbar(
      controller: _controller,
      child: Background(
        width: sizingInformation.isMobileView(context) ? 50 : 100,
        child: Center(
          child: Container(
            child: SingleChildScrollView(
              controller: _controller,
              child: Column(
                children: _content(screenHeight),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CloudFunctionsBloc>(
      create: (context) {
        var firebaseFunctions =
            FirebaseFunctions.instanceFor(app: FirebaseFunctions.instance.app);
        var cloudFunctionsBloc = CloudFunctionsBloc(
          firebaseFunctions,
        );
        return cloudFunctionsBloc;
      },
      child: ResponsiveBuilder(
        builder: (context, sizingInformation) {
          return Scaffold(
            appBar: PreferredSize(
              preferredSize: Size(sizingInformation.screenSize.width,
                  sizingInformation.isMobileView(context) ? 100 : 90),
              child: _navbar,
            ),
            body: _buildScreen(sizingInformation),
          );
        },
      ),
    );
  }
}
