import 'package:fluttech/application.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/circle_page_indicator.dart';
import 'package:fluttech/widgets/circle_placeholder.dart';
import 'package:fluttech/widgets/side_bar.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class OurTeam extends StatelessWidget {
  final ValueChanged onJobApplied;

  const OurTeam({
    Key? key,
    required this.onJobApplied,
  }) : super(key: key);

  Widget _buildDesktopOurTeamContent(BuildContext context) {
    return Center(
      child: Container(
        child: Wrap(
          alignment: WrapAlignment.start,
          runAlignment: WrapAlignment.spaceEvenly,
          spacing: 20.0,
          runSpacing: 20.0,
          children: [
            TeamMember(
              name: S.of(context).team_gyuri_name,
              photoUrl: 'assets/images/gyuri.png',
              description: S.of(context).team_gyuri_description,
            ),
            TeamMember(
              name: S.of(context).team_istvan_name,
              photoUrl: 'assets/images/istvan.png',
              description: S.of(context).team_istvan_description,
            ),
            TeamMember(
              name: S.of(context).team_rudi_name,
              photoUrl: 'assets/images/rudi.png',
              description: S.of(context).team_rudi_description,
            ),
            TeamMember(
              name: S.of(context).team_emanuela_name,
              photoUrl: 'assets/images/ema.png',
              description: S.of(context).team_emanuela_description,
            ),
            TeamMember(
              name: S.of(context).team_ovidiu_name,
              photoUrl: 'assets/images/ovi.png',
              description: S.of(context).team_ovidiu_description,
            ),
            TeamMember(
              name: S.of(context).team_empty_position_name,
              photoUrl: 'assets/images/junior_job.png',
              description: S.of(context).team_empty_junior_position_description,
              applyFor: S.of(context).team_apply_junior,
              onJobApplied: onJobApplied,
            ),
            TeamMember(
              name: S.of(context).team_empty_position_name,
              photoUrl: 'assets/images/senior_job.png',
              description: S.of(context).team_empty_senior_position_description,
              applyFor: S.of(context).team_apply_senior,
              onJobApplied: onJobApplied,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDesktopOurTeam(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: LEFT_PADDING,
            child: SideBar(
              text: S.of(context).team,
              activeIndex: 1,
            ),
          ),
          Expanded(
            child: _buildDesktopOurTeamContent(context),
          ),
          SizedBox(
            width: BACKGROUND_MARGIN,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        if (sizingInformation.isSmallScreen()) {
          return OurTeamOnMobile(
            onJobApplied: onJobApplied,
          );
        }
        return _buildDesktopOurTeam(context);
      },
    );
  }
}

class OurTeamOnMobile extends StatefulWidget {
  final ValueChanged onJobApplied;

  const OurTeamOnMobile({
    Key? key,
    required this.onJobApplied,
  }) : super(key: key);

  @override
  _OurTeamOnMobileState createState() => _OurTeamOnMobileState();
}

class _OurTeamOnMobileState extends State<OurTeamOnMobile> {
  final _currentPageNotifier = ValueNotifier<int>(0);
  PageController _controller = PageController(
    initialPage: 0,
    viewportFraction: 1.0,
  );

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget _buildCircleIndicator() {
    return Positioned(
      left: 0.0,
      right: 0.0,
      bottom: 0.0,
      child: Padding(
        padding: EdgeInsets.all(12.0),
        child: CirclePageIndicator(
          itemCount: 7,
          selectedDotColor: Colors.black,
          dotColor: Color(0xFFBFBFBF),
          currentPageNotifier: _currentPageNotifier,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        var localWith = sizingInformation.localWidgetSize.width;
        var localPadding = (localWith - 350) / 2;
        return Container(
          height: 400,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                width: BACKGROUND_MARGIN / 2,
                alignment: Alignment.center,
                child: RotatedBox(
                  quarterTurns: -1,
                  child: Text(
                    S.of(context).team,
                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                  ),
                ),
              ),
              Stack(
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: 32.0),
                    child: Container(
                      width: sizingInformation.screenSize.width -
                          BACKGROUND_MARGIN,
                      child: PageView(
                        controller: _controller,
                        onPageChanged: (int index) {
                          setState(() {
                            _currentPageNotifier.value = index;
                          });
                        },
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                              left: localPadding,
                              right: localPadding,
                            ),
                            child: TeamMember(
                              strokeWidth: STROKE_WIDTH,
                              showContent: _currentPageNotifier.value == 0,
                              name: S.of(context).team_gyuri_name,
                              photoUrl: 'assets/images/gyuri.png',
                              description: S.of(context).team_gyuri_description,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: localPadding,
                              right: localPadding,
                            ),
                            child: TeamMember(
                              strokeWidth: STROKE_WIDTH,
                              showContent: _currentPageNotifier.value == 1,
                              name: S.of(context).team_istvan_name,
                              photoUrl: 'assets/images/istvan.png',
                              description:
                                  S.of(context).team_istvan_description,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: localPadding,
                              right: localPadding,
                            ),
                            child: TeamMember(
                              strokeWidth: STROKE_WIDTH,
                              showContent: _currentPageNotifier.value == 2,
                              name: S.of(context).team_rudi_name,
                              photoUrl: 'assets/images/rudi.png',
                              description: S.of(context).team_rudi_description,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: localPadding,
                              right: localPadding,
                            ),
                            child: TeamMember(
                              strokeWidth: STROKE_WIDTH,
                              showContent: _currentPageNotifier.value == 3,
                              name: S.of(context).team_emanuela_name,
                              photoUrl: 'assets/images/ema.png',
                              description:
                                  S.of(context).team_emanuela_description,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: localPadding,
                              right: localPadding,
                            ),
                            child: TeamMember(
                              strokeWidth: STROKE_WIDTH,
                              showContent: _currentPageNotifier.value == 4,
                              name: S.of(context).team_ovidiu_name,
                              photoUrl: 'assets/images/ovi.png',
                              description:
                                  S.of(context).team_ovidiu_description,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: localPadding,
                              right: localPadding,
                            ),
                            child: TeamMember(
                              strokeWidth: STROKE_WIDTH,
                              showContent: _currentPageNotifier.value == 5,
                              name: S.of(context).team_empty_position_name,
                              photoUrl: 'assets/images/junior_job.png',
                              description: S
                                  .of(context)
                                  .team_empty_junior_position_description,
                              applyFor: S.of(context).team_apply_junior,
                              onJobApplied: widget.onJobApplied,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: localPadding,
                              right: localPadding,
                            ),
                            child: TeamMember(
                              strokeWidth: STROKE_WIDTH,
                              showContent: _currentPageNotifier.value == 6,
                              name: S.of(context).team_empty_position_name,
                              photoUrl: 'assets/images/senior_job.png',
                              description: S
                                  .of(context)
                                  .team_empty_senior_position_description,
                              applyFor: S.of(context).team_apply_senior,
                              onJobApplied: widget.onJobApplied,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  _buildCircleIndicator(),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}

class TeamMember extends StatefulWidget {
  final double strokeWidth;
  final bool showContent;
  final String photoUrl;
  final String name;
  final String description;
  final String? applyFor;
  final ValueChanged? onJobApplied;

  const TeamMember({
    Key? key,
    this.strokeWidth = STROKE_WIDTH,
    this.showContent = false,
    required this.photoUrl,
    required this.name,
    required this.description,
    this.applyFor,
    this.onJobApplied,
  }) : super(key: key);

  @override
  _TeamMemberState createState() => _TeamMemberState();
}

class _TeamMemberState extends State<TeamMember> {
  bool _mouseHover = false;

  Widget _buildProfilePicture() {
    return ProfilePicture(
      radius: 40,
      photoUrl: widget.photoUrl,
    );
  }

  Widget _buildTeamMemberProfile() {
    return Container(
      key: ValueKey(widget.photoUrl),
      width: 250,
      height: 270,
      child: Stack(
        children: [
          Positioned(
            top: 55,
            left: 10,
            right: 10,
            bottom: 10,
            child: PhysicalModel(
              color: Colors.transparent,
              shadowColor: Colors.grey,
              elevation: 10.0,
              child: Container(
                width: 250,
                height: 270,
                padding: EdgeInsets.only(
                  left: 20.0,
                  top: 43,
                  right: 30,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Color(0xFFCB4CFD),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SelectableText(
                          widget.name,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        SelectableText(
                          widget.description,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    if (widget.applyFor != null)
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: TextButton(
                          style: TextButton.styleFrom(
                            primary: Colors.black87,
                            backgroundColor: Colors.white,
                            padding: EdgeInsets.all(15.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(8.0),
                              ),
                            ),
                          ),
                          onPressed: () => widget.onJobApplied!(widget.applyFor!),
                          child: Text(
                            widget.applyFor!,
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            left: 20,
            top: 5,
            child: Container(
              width: 80.0,
              height: 80.0,
              // color: Colors.white,
              child: _buildProfilePicture(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildEmptyProfile() {
    return Container(
      key: UniqueKey(),
      width: 250,
      height: 270,
      child: Stack(
        children: [
          Positioned(
            top: 55,
            left: 10,
            right: 10,
            bottom: 10,
            child: PhysicalModel(
              color: Colors.transparent,
              shadowColor: Colors.grey,
              elevation: 10.0,
              child: Container(
                width: 250,
                height: 270,
                padding: const EdgeInsets.only(
                  left: 19.0,
                  top: 63,
                  right: 40,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Color(0xFFF4F4F4),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.black,
                      width: 115,
                      height: widget.strokeWidth,
                    ),
                    SizedBox(
                      height: 35,
                    ),
                    Container(
                      color: Colors.black,
                      width: 135,
                      height: widget.strokeWidth,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      color: Colors.black,
                      width: 180,
                      height: widget.strokeWidth,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      color: Colors.black,
                      width: 150,
                      height: widget.strokeWidth,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      color: Colors.black,
                      width: 115,
                      height: widget.strokeWidth,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            left: 20,
            top: 5,
            child: Container(
              width: 80.0,
              height: 80.0,
              color: Colors.transparent,
              child: CirclePlaceholder(
                // color: Color(0xFF000000),
                strokeWidth: widget.strokeWidth,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (_) {
        if (!widget.showContent && _mouseHover != true) {
          setState(() {
            _mouseHover = true;
          });
        }
      },
      onTapUp: (_) {
        if (!widget.showContent && _mouseHover != true) {
          setState(() {
            _mouseHover = false;
          });
        }
      },
      child: MouseRegion(
        onEnter: (_) {
          if (!widget.showContent) {
            setState(() {
              _mouseHover = true;
            });
          }
        },
        onExit: (_) {
          if (!widget.showContent) {
            setState(() {
              _mouseHover = false;
            });
          }
        },
        child: AnimatedSwitcher(
          duration: Duration(milliseconds: 300),
          reverseDuration: Duration(milliseconds: 100),
          child: widget.showContent || _mouseHover
              ? _buildTeamMemberProfile()
              : _buildEmptyProfile(),
        ),
      ),
    );
  }
}

class ProfilePicture extends StatelessWidget {
  final double radius;
  final String photoUrl;

  const ProfilePicture({
    Key? key,
    required this.radius,
    required this.photoUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10.0,
      shadowColor: Colors.white,
      margin: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: Colors.grey.shade500,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(45),
      ),
      child: CircleAvatar(
        backgroundColor: Colors.white,
        backgroundImage: AssetImage(
          photoUrl,
        ),
        radius: radius,
      ),
    );
  }
}
