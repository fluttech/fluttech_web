class ProjectDetails {
  final String title;
  final String clientNameShort;
  final String clientWebSite;
  final String description;
  final String logoUrl;
  final String screenUrl;
  final String webSiteUrl;
  final String? playStoreUrl;
  final String? appleStoreUrl;
  final String? clientName;
  final String? review;

  ProjectDetails({
    required this.title,
    required this.clientWebSite,
    required this.description,
    required this.logoUrl,
    required this.screenUrl,
    required this.webSiteUrl,
    this.playStoreUrl,
    this.appleStoreUrl,
    this.clientName,
    this.review,
    this.clientNameShort = '',
  });
}
