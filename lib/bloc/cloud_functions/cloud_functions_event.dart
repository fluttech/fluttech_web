part of 'cloud_functions_bloc.dart';

abstract class CloudFunctionsEvent extends Equatable {
  const CloudFunctionsEvent();

  @override
  bool get stringify => false;
}

class SendEmail extends CloudFunctionsEvent {
  final String email;
  final String subject;
  final String message;

  SendEmail({
    required this.email,
    required this.subject,
    required this.message,
  });

  @override
  List<String> get props => [
        email,
        subject,
        message,
      ];
}
