part of 'cloud_functions_bloc.dart';

abstract class CloudFunctionsState extends Equatable {
  const CloudFunctionsState();

  @override
  List<Object> get props => [];
}

class StandBy extends CloudFunctionsState {}

class EmailSent extends CloudFunctionsState {}

class SendingEmail extends CloudFunctionsState {}

class FailedSendingEmail extends CloudFunctionsState {}
