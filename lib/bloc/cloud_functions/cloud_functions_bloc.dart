import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:equatable/equatable.dart';
import 'package:logging/logging.dart';

part 'cloud_functions_event.dart';
part 'cloud_functions_state.dart';

class CloudFunctionsBloc
    extends Bloc<CloudFunctionsEvent, CloudFunctionsState> {
  final Logger _log = Logger('CloudFunctionsBloc');
  final FirebaseFunctions _firebaseFunctions;

  CloudFunctionsBloc(this._firebaseFunctions) : super(StandBy());

  @override
  Stream<CloudFunctionsState> mapEventToState(
    CloudFunctionsEvent event,
  ) async* {
    if (event is SendEmail) {
      yield* _mapSendEmailToState(
        email: event.email,
        subject: event.subject,
        message: event.message,
      );
    }
  }

  Stream<CloudFunctionsState> _mapSendEmailToState({
    required String email,
    required String subject,
    required String message,
  }) async* {
    yield SendingEmail();

    _log.info('Sending email from $email...');
    try {
      HttpsCallable sendFeedbackFunction = _firebaseFunctions.httpsCallable(
        'sendMail',
      );

      await sendFeedbackFunction.call({
        'email': email,
        'subject': subject,
        'message': message,
      });
      _log.info('Email sent successfully');

      yield EmailSent();
      yield StandBy();
    } catch (error, stacktrace) {
      _log.info('Email doesn\'t sent: $stacktrace \n $error');
      yield FailedSendingEmail();
      yield StandBy();
    }
  }
}
