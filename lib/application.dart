import 'package:firebase_core/firebase_core.dart';
import 'package:fluttech/application_router_delegate.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations_en.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:logging/logging.dart';

void startApplication() async {
  var applicationLauncher = createApplicationLauncher(
    application: FluttechWebApplication(),
  );

  Logger.root.level = Level.SEVERE; // defaults to Level.INFO
  Logger.root.onRecord.listen((LogRecord rec) {
    print(
        '${rec.level.name[0]}: ${rec.loggerName}: ${rec.time}: ${rec.message}');
    if (rec.error != null) {
      print(
          '${rec.level.name[0]}: ${rec.loggerName}: ${rec.time}: ${rec.error}');
    }

    if (rec.stackTrace != null) {
      print(
          '${rec.level.name[0]}: ${rec.loggerName}: ${rec.time}: ${rec.stackTrace}');
    }
  });

  await Firebase.initializeApp();

  runApp(applicationLauncher);
}

Widget createApplicationLauncher({
  required Widget application,
}) {
  return ApplicationLauncher(
    application: application,
  );
}

class ApplicationLauncher extends StatefulWidget {
  final Widget application;

  ApplicationLauncher({
    Key? key,
    required this.application,
  }) : super(key: key);

  @override
  _ApplicationLauncherState createState() => _ApplicationLauncherState();
}

class _ApplicationLauncherState extends State<ApplicationLauncher>
    with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precachePicture(
      ExactAssetPicture(SvgPicture.svgStringDecoderBuilder,
          'assets/images/fluttech_logo_v1.svg'),
      context,
    );
    precacheImage(AssetImage('assets/images/ema.png'), context);
    precacheImage(AssetImage('assets/images/gyuri.png'), context);
    precacheImage(AssetImage('assets/images/istvan.png'), context);
    precacheImage(AssetImage('assets/images/ovi.png'), context);
    precacheImage(AssetImage('assets/images/rudi.png'), context);
    precacheImage(AssetImage('assets/images/senior_job.png'), context);
    precacheImage(AssetImage('assets/images/junior_job.png'), context);
    precacheImage(AssetImage('assets/images/binaris_logo.png'), context);
    precacheImage(AssetImage('assets/images/deescus_logo.png'), context);
    precacheImage(AssetImage('assets/images/evenito_logo.png'), context);
    precacheImage(AssetImage('assets/images/hashi_logo.png'), context);
    precacheImage(AssetImage('assets/images/hyll_logo.png'), context);
    precacheImage(AssetImage('assets/images/ozonvicc_logo.png'), context);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.application;
  }
}

class FluttechWebApplication extends StatelessWidget {
  final ApplicationRouterDelegate applicationRouterDelegate =
      ApplicationRouterDelegate();
  final ApplicationRouteInformationParser applicationRouteInformationParser =
      ApplicationRouteInformationParser();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Fluttech',
      theme: ThemeData(fontFamily: "RedHatText"),
      routerDelegate: applicationRouterDelegate,
      routeInformationParser: applicationRouteInformationParser,
      debugShowCheckedModeBanner: false,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
    );
  }
}

class S {
  static AppLocalizations of(BuildContext context) {
    return AppLocalizations.of(context) ?? AppLocalizationsEn();
  }
}
