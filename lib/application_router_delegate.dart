import 'package:fluttech/pages/about/about_page.dart';
import 'package:fluttech/pages/home/home_page.dart';
import 'package:fluttech/widgets/navbar/navbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ApplicationRouterDelegate extends RouterDelegate<FluttechRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<FluttechRoutePath> {
  final GlobalKey<NavigatorState> _navigatorKey;

  ApplicationRouterDelegate() : _navigatorKey = GlobalKey<NavigatorState>();

  @override
  GlobalKey<NavigatorState> get navigatorKey => _navigatorKey;

  FluttechRoutePath get currentConfiguration => _currentConfiguration;

  FluttechRoutePath _currentConfiguration = FluttechRoutePath.home();

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        Widget navBar = NavBar(
          page:
              _currentConfiguration.isHome ? HomePage.route : AboutUsPage.route,
          onPageTap: (newPage) {
            switch (newPage) {
              case HomePage.route:
                _currentConfiguration = FluttechRoutePath.home();
                break;
              case AboutUsPage.route:
                _currentConfiguration = FluttechRoutePath.aboutUs();
                break;
              default:
                _currentConfiguration = FluttechRoutePath.home();
                break;
            }
            notifyListeners();
          },
        );
        return Navigator(
          key: _navigatorKey,
          pages: [
            if (_currentConfiguration.isHome)
              HomePage(
                navBar: navBar,
              ),
            if (_currentConfiguration.isAboutUs)
              AboutUsPage(
                navBar: navBar,
              ),
          ],
          onPopPage: (route, result) {
            if (!route.didPop(result)) {
              return false;
            }

            notifyListeners();

            return true;
          },
        );
      },
    );
  }

  @override
  Future<void> setNewRoutePath(FluttechRoutePath path) async {
    // parse the path and update the navigator's state
    _currentConfiguration = path;
  }
}

class ApplicationRouteInformationParser
    extends RouteInformationParser<FluttechRoutePath> {
  @override
  Future<FluttechRoutePath> parseRouteInformation(
    RouteInformation routeInformation,
  ) async {
    var location = routeInformation.location;
    if (location == null) {
      return FluttechRoutePath.home();
    }

    final uri = Uri.parse(location);

    switch (uri.path) {
      case '/':
        return FluttechRoutePath.home();
      case HomePage.route:
        return FluttechRoutePath.home();
      case AboutUsPage.route:
        return FluttechRoutePath.aboutUs();
      default:
        throw 'Unknown page';
    }
  }

  @override
  RouteInformation? restoreRouteInformation(FluttechRoutePath path) {
    if (path.isHome) {
      return RouteInformation(location: HomePage.route);
    }
    if (path.isAboutUs) {
      return RouteInformation(location: AboutUsPage.route);
    }
    return null;
  }
}

@immutable
class FluttechRoutePath {
  final bool _isAboutUs;
  final bool _isHome;

  FluttechRoutePath(this._isAboutUs, this._isHome);

  FluttechRoutePath.aboutUs()
      : _isAboutUs = true,
        _isHome = false;

  FluttechRoutePath.home()
      : _isAboutUs = false,
        _isHome = true;

  bool get isAboutUs => _isAboutUs;

  bool get isHome => _isHome;
}
