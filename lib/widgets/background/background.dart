import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final color;
  final BlendMode? blendMode;
  final Widget? child;
  final double? width;
  final double? height;

  const Background({
    Key? key,
    this.color,
    this.blendMode,
    this.width,
    this.height,
    this.child,
  }) : super(key: key);

  Widget _buildBackground() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: [
        Container(
          width: width,
          color: Color(0xFFEEF2F5),
        ),
        Expanded(
          child: Container(
            color: Color(0xFFFFFFFF),
          ),
        ),
        Container(
          width: width,
          color: Color(0xFFEEF2F5),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    bool filtered = color != null;
    if (filtered) {
      return Stack(
        children: [
          Opacity(
            opacity: 0.5,
            child: Container(
              height: height,
              foregroundDecoration: BoxDecoration(
                color: color,
                backgroundBlendMode: blendMode,
              ),
              child: _buildBackground(),
            ),
          ),
          if (child != null) child!,
        ],
      );
    } else
      return Stack(
        children: [
          _buildBackground(),
          if (child != null) child!,
        ],
      );
  }
}
