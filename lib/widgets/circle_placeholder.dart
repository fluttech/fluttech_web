import 'package:flutter/material.dart';

class _PlaceholderPainter extends CustomPainter {
  const _PlaceholderPainter({
    required this.color,
    required this.strokeWidth,
  });

  final Color color;
  final double strokeWidth;

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = color
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;
    canvas.drawOval(
      Rect.fromLTWH(0, 0, size.width, size.height),
      paint,
    );

    canvas.drawLine(Offset(size.width / 2, size.height / 2),
        Offset(size.width / 6, size.height / 6), paint);
    canvas.drawLine(Offset(size.width / 2, size.height / 2),
        Offset(size.width / 6 * 5, size.height / 6), paint);
    canvas.drawLine(Offset(size.width / 2, size.height / 2),
        Offset(size.width / 6, size.height / 6 * 5), paint);
    canvas.drawLine(Offset(size.width / 2, size.height / 2),
        Offset(size.width / 6 * 5, size.height / 6 * 5), paint);
  }

  @override
  bool shouldRepaint(_PlaceholderPainter oldPainter) {
    return oldPainter.color != color || oldPainter.strokeWidth != strokeWidth;
  }

  @override
  bool hitTest(Offset position) => false;
}

class CirclePlaceholder extends StatelessWidget {
  final Color color;

  final double strokeWidth;

  final double fallbackWidth;

  final double fallbackHeight;

  const CirclePlaceholder({
    Key? key,
    this.color = const Color(0xFF455A64),
    this.strokeWidth = 2.0,
    this.fallbackWidth = 400.0,
    this.fallbackHeight = 400.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LimitedBox(
      maxWidth: fallbackWidth,
      maxHeight: fallbackHeight,
      child: CustomPaint(
        size: Size.infinite,
        foregroundPainter: _PlaceholderPainter(
          color: color,
          strokeWidth: strokeWidth,
        ),
      ),
    );
  }
}
