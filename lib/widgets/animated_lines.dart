import 'package:fluttech/application.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:flutter/material.dart';

class ServicesAndProducts extends StatelessWidget {
  final CrossAxisAlignment crossAxisAlignment;
  final MainAxisAlignment mainAxisAlignment;

  final List<String> _services = [
    'development: ',
    'performance: ',
    'platform: ',
    'style: ',
  ];

  final List<String> _products = [
    'fast;',
    'light;',
    'universal;',
    'trendy;',
  ];

  ServicesAndProducts({
    Key? key,
    required this.crossAxisAlignment,
    required this.mainAxisAlignment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        var maxWidth = constraints.maxWidth;
        var distance = constraints.maxHeight / 40;
        return Column(
          mainAxisAlignment: mainAxisAlignment,
          crossAxisAlignment: crossAxisAlignment,
          children: [
            SelectableText(
              '${S.of(context).services} (',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 17),
            ),
            SizedBox(height: distance),
            AnimatedLine(
              color: Colors.black,
              width: maxWidth - maxWidth / 4,
              maxWidth: maxWidth,
              height: STROKE_WIDTH,
              service: _services[0],
              product: _products[0],
            ),
            SizedBox(height: distance),
            AnimatedLine(
              color: Colors.black,
              width: maxWidth - maxWidth / 12,
              maxWidth: maxWidth,
              height: STROKE_WIDTH,
              service: _services[1],
              product: _products[1],
            ),
            SizedBox(height: distance),
            AnimatedLine(
              color: Colors.black,
              width: maxWidth - maxWidth / 3,
              maxWidth: maxWidth,
              height: STROKE_WIDTH,
              service: _services[2],
              product: _products[2],
            ),
            SizedBox(height: distance),
            AnimatedLine(
              color: Colors.black,
              width: maxWidth,
              maxWidth: maxWidth,
              height: STROKE_WIDTH,
              service: _services[3],
              product: _products[3],
            ),
            SizedBox(height: distance),
            Text(
              ');',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 17),
            ),
          ],
        );
      },
    );
  }
}

class AllPlatforms extends StatelessWidget {
  final CrossAxisAlignment crossAxisAlignment;
  final MainAxisAlignment mainAxisAlignment;

  final List<String> _services = [
    'ios: ',
    'android: ',
    'mac: ',
    'windows: ',
    'web: ',
    'linux: ',
  ];

  AllPlatforms({
    Key? key,
    required this.crossAxisAlignment,
    required this.mainAxisAlignment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        var maxWidth = constraints.maxWidth;
        var distance = constraints.maxHeight / 40;
        return Column(
          mainAxisAlignment: mainAxisAlignment,
          crossAxisAlignment: crossAxisAlignment,
          children: [
            SelectableText(
              '${S.of(context).platforms} (',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 17),
            ),
            SizedBox(height: distance),
            AnimatedLine(
              color: Colors.black,
              width: maxWidth - maxWidth / 4,
              maxWidth: maxWidth,
              height: STROKE_WIDTH,
              service: _services[0],
              product: 'true;',
            ),
            SizedBox(height: distance),
            AnimatedLine(
              color: Colors.black,
              width: maxWidth - maxWidth / 12,
              maxWidth: maxWidth,
              height: STROKE_WIDTH,
              service: _services[1],
              product: 'true;',
            ),
            SizedBox(height: distance),
            AnimatedLine(
              color: Colors.black,
              width: maxWidth - maxWidth / 3,
              maxWidth: maxWidth,
              height: STROKE_WIDTH,
              service: _services[2],
              product: 'true;',
            ),
            SizedBox(height: distance),
            AnimatedLine(
              color: Colors.black,
              width: maxWidth,
              maxWidth: maxWidth,
              height: STROKE_WIDTH,
              service: _services[3],
              product: 'true;',
            ),
            SizedBox(height: distance),
            AnimatedLine(
              color: Colors.black,
              width: maxWidth - maxWidth / 10,
              maxWidth: maxWidth,
              height: STROKE_WIDTH,
              service: _services[4],
              product: 'true;',
            ),
            SizedBox(height: distance),
            AnimatedLine(
              color: Colors.black,
              width: maxWidth - maxWidth / 4,
              maxWidth: maxWidth,
              height: STROKE_WIDTH,
              service: _services[5],
              product: 'true;',
            ),
            SizedBox(height: distance),
            Text(
              ');',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 17),
            ),
          ],
        );
      },
    );
  }
}

class AnimatedLine extends StatefulWidget {
  final Color color;
  final double width;
  final double height;
  final double maxWidth;
  final String service;
  final String product;

  const AnimatedLine({
    Key? key,
    required this.color,
    required this.width,
    required this.height,
    required this.maxWidth,
    this.service = '',
    this.product = '',
  }) : super(key: key);

  @override
  _AnimatedLineState createState() => _AnimatedLineState();
}

class _AnimatedLineState extends State<AnimatedLine> {
  bool _mouseHover = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onHover: (value) {
        setState(() {
          _mouseHover = value;
        });
      },
      onTapDown: (_) {
        setState(() {
          _mouseHover = true;
        });
      },
      onTapCancel: () {
        setState(() {
          _mouseHover = false;
        });
      },
      onTap: () => {},
      child: AnimatedSwitcher(
        duration: Duration(milliseconds: 400),
        reverseDuration: Duration(milliseconds: 200),
        child: !_mouseHover
            ? Container(
                key: ValueKey('line_${widget.product}'),
                width: widget.maxWidth,
                height: 3.7 * widget.height,
                padding: EdgeInsets.only(left: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      color: widget.color,
                      width: widget.width,
                      height: widget.height,
                    ),
                  ],
                ),
              )
            : Container(
                key: ValueKey(widget.product),
                width: widget.maxWidth,
                height: 3.7 * widget.height,
                padding: EdgeInsets.only(left: 15),
                child: Row(
                  children: [
                    SelectableText(
                      widget.service,
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 13,
                      ),
                    ),
                    SelectableText(
                      widget.product,
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: 13,
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
