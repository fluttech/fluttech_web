import 'package:fluttech/application.dart';
import 'package:fluttech/widgets/app_drawer/drawer_option.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'drawer_mobile.dart';

class WebDrawer extends StatelessWidget {
  const WebDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: AppDrawerMobile(),
    );
  }

  static List<Widget> getDrawerOptions(BuildContext context) {
    return [
      DrawerOption(
        title: 'Home',
      ),
      DrawerOption(
        title: 'About',
      ),
      DrawerOption(
        title: S.of(context).services,
      ),
      DrawerOption(
        title: 'Projects',
      ),
    ];
  }
}
