import 'package:flutter/material.dart';

class DrawerOptionMobilePortrait extends StatelessWidget {
  final String title;
  final IconData? iconData;

  const DrawerOptionMobilePortrait({
    Key? key,
    required this.title,
    this.iconData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 25),
      height: 80,
      child: Row(
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: 30),
          )
        ],
      ),
    );
  }
}
