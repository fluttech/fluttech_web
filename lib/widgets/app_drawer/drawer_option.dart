import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'drawer_option_mobile.dart';

class DrawerOption extends StatelessWidget {
  final String title;

  const DrawerOption({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayoutBuilder(
        portrait: (context) => DrawerOptionMobilePortrait(
          title: title,
        ),
      ),
    );
  }
}
