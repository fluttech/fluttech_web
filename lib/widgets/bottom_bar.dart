import 'dart:math';

import 'package:fluttech/application.dart';
import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/link.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:url_launcher/url_launcher.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({
    Key? key,
  }) : super(key: key);

  Widget _buildBottomBar(
    BuildContext context,
    SizingInformation sizingInformation,
  ) {
    var isMobileView = sizingInformation.isMobileView(context);
    return Container(
      color: Colors.blueGrey[900],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            width:
                min(MAX_WIDTH / 3 * 2, sizingInformation.localWidgetSize.width),
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Column(
              children: [
                SelectableText(
                  S.of(context).critbiz,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    // fontWeight: FontWeight.w500,
                    fontSize: 9,
                    color: Colors.white,
                  ),
                ),
                SizedBox(height: 20.0),
                Wrap(
                  spacing: 40,
                  runSpacing: 40,
                  alignment: WrapAlignment.center,
                  runAlignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    CustomLink(
                      url: 'http://critbiz.ro',
                      isMobileView: isMobileView,
                      child: InkWell(
                        child: Container(
                          height: 50,
                          child: Image.asset(
                            'assets/images/logo_critbiz.png',
                            color: Colors.white,
                          ),
                        ),
                        onTap: () => launch('http://critbiz.ro'),
                      ),
                    ),
                    CustomLink(
                      url: 'https://europa.eu/european-union/index_ro',
                      isMobileView: isMobileView,
                      child: InkWell(
                        child: Container(
                          height: 80,
                          child: Image.asset(
                            'assets/images/logo_ue.png',
                          ),
                        ),
                        onTap: () =>
                            launch('https://europa.eu/european-union/index_ro'),
                      ),
                    ),
                    CustomLink(
                      url: 'https://www.gov.ro/',
                      isMobileView: isMobileView,
                      child: InkWell(
                        child: Container(
                          height: 80,
                          child: Image.asset(
                            'assets/images/guv_ro.png',
                          ),
                        ),
                        onTap: () => launch('https://www.gov.ro/'),
                      ),
                    ),
                    CustomLink(
                      url: 'https://www.fonduri-ue.ro/',
                      isMobileView: isMobileView,
                      child: InkWell(
                        child: Container(
                          height: 80,
                          child: Image.asset(
                            'assets/images/instr_struct.png',
                          ),
                        ),
                        onTap: () => launch('https://www.fonduri-ue.ro/'),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 50.0),
                Container(
                  alignment: Alignment.center,
                  child: SelectableText(
                    S.of(context).copyright,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        return _buildBottomBar(context, sizingInformation);
      },
    );
  }
}
