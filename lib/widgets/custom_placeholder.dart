import 'package:flutter/material.dart';

class CustomPlaceHolder extends StatelessWidget {
  final Color color;
  final Color shadowColor;
  final double strokeWidth;
  final double fallbackWidth;
  final double fallbackHeight;

  const CustomPlaceHolder({
    Key? key,
    required this.color,
    required this.shadowColor,
    required this.strokeWidth,
    this.fallbackWidth = 400.0,
    required this.fallbackHeight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PhysicalModel(
      color: shadowColor != null? Colors.white : Colors.transparent,
      shadowColor: shadowColor != null? shadowColor : Colors.transparent,
      elevation: 15.0,
      child: Container(
        // color: Colors.white,
        child: Placeholder(
          color: color,
          strokeWidth: strokeWidth,
          fallbackWidth: fallbackWidth,
          fallbackHeight: fallbackHeight,
        ),
      ),
    );
  }
}
