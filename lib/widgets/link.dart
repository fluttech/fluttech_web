import 'package:flutter/material.dart';
import 'package:url_launcher/link.dart';

class CustomLink extends StatelessWidget {
  final Widget child;
  final String url;
  final bool isMobileView;

  const CustomLink({
    Key? key,
    required this.child,
    required this.url,
    required this.isMobileView,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isMobileView) {
      return child;
    }
    return Link(
      uri: Uri.parse(url),
      target: LinkTarget.blank,
      builder: (context, followLink) {
        return child;
      },
    );
  }
}
