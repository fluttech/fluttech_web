import 'package:flutter/material.dart';

class AnimatedButton extends StatefulWidget {
  final double width;
  final double height;
  final double borderSize;
  final VoidCallback? onPressed;
  final String text;
  final double textSize;

  const AnimatedButton({
    Key? key,
    required this.width,
    required this.height,
    required this.text,
    required this.textSize,
    required this.borderSize,
    this.onPressed,
  }) : super(key: key);

  @override
  _AnimatedButtonState createState() => _AnimatedButtonState();
}

class _AnimatedButtonState extends State<AnimatedButton> {
  bool _mouseHover = false;

  @override
  void didUpdateWidget(covariant AnimatedButton oldWidget) {
    super.didUpdateWidget(oldWidget);
    if(oldWidget.onPressed != null && widget.onPressed == null){
      setState(() {
        _mouseHover = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var color = _mouseHover ? Color(0xFFCB4CFD) : Colors.black;
    var border = _mouseHover
        ? Border.all(color: color, width: widget.borderSize / 2)
        : Border.all(color: color, width: widget.borderSize);

    return MouseRegion(
      onEnter: (event) {
        if (widget.onPressed == null) {
          return;
        }
        setState(() {
          _mouseHover = true;
        });
      },
      onExit: (event) {
        if (widget.onPressed == null) {
          return;
        }
        setState(() {
          _mouseHover = false;
        });
      },
      child: PhysicalModel(
        color: Colors.white,
        elevation: 15.0,
        borderRadius: BorderRadius.circular(51),
        child: AnimatedContainer(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(51),
            border: border,
          ),
          duration: Duration(milliseconds: 200),
          curve: Curves.fastOutSlowIn,
          child: MaterialButton(
            onPressed: widget.onPressed,
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(51),
            ),
            child: Text(
              widget.text,
              style: TextStyle(
                  color: color,
                  fontWeight: FontWeight.w700,
                  fontSize: widget.textSize),
            ),
          ),
        ),
      ),
    );
  }
}
