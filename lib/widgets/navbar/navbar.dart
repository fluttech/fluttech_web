import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:fluttech/widgets/animated_button.dart';
import 'package:fluttech/widgets/background/background.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_builder/responsive_builder.dart';

class NavBar extends StatelessWidget {
  final String page;
  final ValueChanged? onPageTap;
  final VoidCallback? onContactPressed;

  const NavBar({
    Key? key,
    required this.page,
    this.onPageTap,
    this.onContactPressed,
  }) : super(key: key);

  Widget _buildDesktopNavBar(BuildContext context) {
    return Center(
      child: Container(
        // width: MAX_WIDTH,
        alignment: Alignment.topCenter,
        child: Background(
          width: BACKGROUND_MARGIN,
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 20.0,
              horizontal: RIGHT_PADDING,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () => {
                    // todo navigate to home page
                  },
                  child: NavBarLogoDesktop(),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      // NavBarButton(
                      //   name: 'home',
                      //   isCurrentPage: page == HomePage.route,
                      //   onTap: () => onPageTap(HomePage.route),
                      // ),
                      // SizedBox(width: 10),
                      // NavBarButton(
                      //   name: 'about',
                      //   isCurrentPage: page == AboutUsPage.route,
                      //   onTap: () => onPageTap(AboutUsPage.route),
                      // ),
                      SizedBox(width: 10),
                      AnimatedButton(
                        width: 150,
                        height: 55,
                        borderSize: STROKE_WIDTH,
                        text: 'Contact',
                        textSize: 16,
                        onPressed: () => onContactPressed?.call(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildMobileNavBar(BuildContext context) {
    return Background(
      width: BACKGROUND_MARGIN / 2,
      child: Container(
        padding: EdgeInsets.only(top: 25, left: 25, right: 25),
        child: Row(
          children: [
            NavBarLogoMobile(),
            Spacer(),
            AnimatedButton(
              width: 100,
              height: 35,
              borderSize: STROKE_WIDTH,
              text: 'Contact',
              textSize: 14,
              onPressed: () => onContactPressed?.call(),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        if (!sizingInformation.isMobileView(context)) {
          return _buildDesktopNavBar(context);
        } else {
          return _buildMobileNavBar(context);
        }
      },
    );
  }
}

class NavBarLogoDesktop extends StatelessWidget {
  const NavBarLogoDesktop({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: 220,
      child: SvgPicture.asset(
        'assets/images/fluttech_logo_v1.svg',
      ),
    );
  }
}

class NavBarLogoMobile extends StatelessWidget {
  const NavBarLogoMobile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: SvgPicture.asset(
        'assets/images/fluttech_logo_v1.svg',
        width: 150,
      ),
    );
  }
}

class NavBarButton extends StatelessWidget {
  final String name;
  final bool isCurrentPage;
  final VoidCallback onTap;

  const NavBarButton({
    Key? key,
    required this.name,
    required this.isCurrentPage,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onTap,
      child: isCurrentPage
          ? Container(
              width: 100,
              child: Text(
                name,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                    color: Colors.black),
              ),
            )
          : Container(
              color: Colors.black,
              width: 100,
              height: STROKE_WIDTH,
            ),
    );
  }
}
