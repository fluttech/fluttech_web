import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:flutter/material.dart';

class AnimatedPlaceHolder extends StatefulWidget {
  final double width;
  final double height;

  const AnimatedPlaceHolder({
    required this.width,
    required this.height,
    Key? key,
  }) : super(key: key);

  @override
  _AnimatedPlaceHolderState createState() => _AnimatedPlaceHolderState();
}

class _AnimatedPlaceHolderState extends State<AnimatedPlaceHolder>
    with TickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = new AnimationController(
      duration: Duration(milliseconds: 300),
      vsync: this,
    )..addListener(() => setState(() {}));
    animation = Tween(begin: -20.0, end: 0.0).animate(controller);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (details)=> controller.forward(),
      onTapUp: (details)=> controller.reverse(),
      child: Container(
        child: Stack(
          children: <Widget>[
            Transform.translate(
              offset: Offset(animation.value, animation.value),
              child: Container(
                width: widget.width,
                height: widget.height,
                color: Color(0xffCB4CFD),
              ),
            ),
            MouseRegion(
              onEnter: (event) {
                controller.forward();
              },
              onExit: (event) {
                controller.reverse();
              },
              child: Placeholder(
                color: Color(0xFF000000),
                fallbackWidth: widget.width,
                fallbackHeight: widget.height,
                strokeWidth: STROKE_WIDTH,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
