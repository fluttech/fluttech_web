import 'package:flutter/material.dart';

class SideBar extends StatelessWidget {
  final String text;
  final int activeIndex;

  const SideBar({
    Key? key,
    required this.text,
    required this.activeIndex,
  }) : super(key: key);

  Color _getColor(int index) {
    return activeIndex == index ? Colors.black : Color(0xffBFBFBF);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        RotatedBox(
          quarterTurns: -1,
          child: SelectableText(
            text,
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 28),
          ),
        ),
        SizedBox(
          height: 30.0,
        ),
        Container(
          width: 10,
          height: 10,
          decoration: BoxDecoration(
            color: _getColor(0),
            shape: BoxShape.circle,
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Container(
          width: 10,
          height: 10,
          decoration: BoxDecoration(
            color: _getColor(1),
            shape: BoxShape.circle,
          ),
        ),
        // SizedBox(
        //   height: 10.0,
        // ),
        // Container(
        //   width: 10,
        //   height: 10,
        //   decoration: BoxDecoration(
        //     color: _getColor(2),
        //     shape: BoxShape.circle,
        //   ),
        // ),
      ],
    );
  }
}
