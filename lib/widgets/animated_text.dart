import 'package:fluttech/utils/sizing_information_extension.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class AnimatedText extends StatefulWidget {
  final VoidCallback onPressed;
  final String text;
  final double textSize;

  const AnimatedText({
    Key? key,
    required this.onPressed,
    required this.text,
    required this.textSize,
  }) : super(key: key);

  @override
  _AnimatedTextState createState() => _AnimatedTextState();
}

class _AnimatedTextState extends State<AnimatedText> {
  bool mouseHover = false;

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        var mobileView = sizingInformation.isMobileView(context);
        var color = mouseHover || mobileView ? Color(0xFFCB4CFD) : Colors.black;
        return InkWell(
          hoverColor: Colors.transparent,
          onTap: () {
            widget.onPressed.call();
          },
          onHover: (value) {
            setState(() {
              mouseHover = value;
            });
          },
          child: Text(
            widget.text,
            style: TextStyle(
              color: color,
              fontWeight: FontWeight.w600,
              fontSize: widget.textSize,
              decoration: mouseHover || mobileView
                  ? TextDecoration.underline
                  : TextDecoration.none,
            ),
          ),
        );
      },
    );
  }
}
