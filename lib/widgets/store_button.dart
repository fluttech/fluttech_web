import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

class StoreButton extends StatelessWidget {
  final String image;
  final double height;
  final String url;

  const StoreButton({
    Key? key,
    required this.image,
    this.height = 44,
    required this.url,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PhysicalModel(
      color: Colors.white,
      elevation: 8.0,
      borderRadius: BorderRadius.circular(10),
      child: InkWell(
        onTap: () => launch(url),
        child: SvgPicture.asset(
          image,
          height: height,
        ),
      ),
    );
  }
}