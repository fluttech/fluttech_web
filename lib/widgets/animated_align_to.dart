import 'package:flutter/material.dart';

class AnimatedAlignWidget extends StatelessWidget {
  final Alignment initialAlignment;
  final Alignment finalAlignment;
  final bool doAnimation;
  final Widget child;
  final VoidCallback? onEnd;

  const AnimatedAlignWidget({
    Key? key,
    required this.initialAlignment,
    required this.finalAlignment,
    required this.doAnimation,
    required this.child,
    this.onEnd,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedAlign(
      duration: Duration(milliseconds: 300),
      alignment: doAnimation ? finalAlignment : initialAlignment,
      onEnd: onEnd,
      child: child,
    );
  }
}
