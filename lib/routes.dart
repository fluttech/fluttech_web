import 'package:flutter/widgets.dart';

class Routes {
  static const String home = '/';
  static const String about = 'about';

  static Route<T> fadeThrough<T>(RouteSettings settings, WidgetBuilder page,
      {int duration = 350}) {
    return PageRouteBuilder<T>(
        settings: settings,
        transitionDuration: Duration(milliseconds: duration),
        pageBuilder: (context, animation, secondaryAnimation) => page(context),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          var curve = Curves.easeIn;

          var curvedAnimation = CurvedAnimation(
            parent: animation,
            curve: curve,
          );

          return FadeTransition(
            // position: tween.animate(curvedAnimation),
            opacity: curvedAnimation,
            child: child,
          );
        });
  }
}
