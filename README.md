# fluttech website

Fluttech website.

## Installing and getting started

You'll need to install flutter, and Android Studio

- Open https://flutter.dev/docs/get-started/install
- Install Flutter
- Run `flutter doctor` until it stops having red Xs at the outer level
	- Spoiler: Android Studio, Xcode
    - Use the commands it hints at to speed things up
- Change to the stable flutter channel `flutter channel stable && flutter upgrade`
- `flutter pub get`

### Web Deployment:
1. Make sure you are on the right branch and version
2. `flutter clean`
3. `flutter build web -t lib/main.dart --web-renderer canvaskit --release`
5. Copy the ./build/web content to ./firebase/public and override everything: `cp -a ./build/web/. ./firebase/public`
    for testing only copy the ./build/web content to ./firebase/public/beta:  `cp -a ./build/web/. ./firebase/public/beta`
6. `cd firebase`
7. `firebase projects:list` -- make sure you are on the right project
8. If not: `firebase use fluttech-ed370`
9. Rerun 7. and make sure you see `(current)` suffix after the project name
10. Only hosting:                                           `firebase deploy --only hosting`
    Everything (hosting,security rules, cloud functions):   `firebase deploy`